set TB "immGen_tb"
set TB_DIR "../tb"

vcom ../components/mux4to1.vhd
vcom ../../alu/components/mux_2to1.vhd
vcom ../components/immGen.vhd
vcom $TB_DIR/$TB.vhd

vsim -t ns -novopt work.$TB
run 1 us

quit -f
