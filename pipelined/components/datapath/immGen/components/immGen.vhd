library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity immGen is
	generic(
		parallelism	: integer := 32		
	);
	port(
		-- input signal
		instruction	: in std_logic_vector(parallelism-1 downto 0);

		-- output signal
		immediate	: out signed(parallelism-1 downto 0)
	);
end entity immGen;

architecture behaviour of immGen is

	component mux_2to1 is 
		port(
			-- input singnals
			sel		: in std_logic;
			in0		: in signed(31 downto 0);
			in1		: in signed(31 downto 0);
			
			-- output signals
			mux_out	: out signed(31 downto 0)
		);
	end component mux_2to1;

	component mux4to1 is 
		port(
			-- input signals
			sel		: in std_logic_vector(1 downto 0);
			in0		: in signed(31 downto 0);
			in1		: in signed(31 downto 0);
			in2		: in signed(31 downto 0);
			in3		: in signed(31 downto 0);

			-- output signals
			muxOut	: out signed(31 downto 0)
		);
	end component mux4to1;


	signal opcode	: std_logic_vector(6 downto 0);
	signal I	: std_logic_vector(parallelism-1 downto 0);
	signal S	: std_logic_vector(parallelism-1 downto 0);
	signal SB	: std_logic_vector(parallelism-1 downto 0);
	signal U	: std_logic_vector(parallelism-1 downto 0);
	signal UJ	: std_logic_vector(parallelism-1 downto 0);

	signal sel1	: std_logic;
	signal sel2	: std_logic_vector(1 downto 0);

	signal I_or_U	: signed(parallelism-1 downto 0);
	signal S_or_U	: signed(parallelism-1 downto 0);
	signal SB_or_UJ	: signed(parallelism-1 downto 0);

begin

	opcode			<= instruction(6 downto 0);
	sel1			<= opcode(2);
	sel2			<= opcode(6 downto 5);

	I(31 downto 12)		<= (others => instruction(31));
	I(11 downto 0)		<= instruction(31 downto 20);

	S(31 downto 12)		<= (others => instruction(31));
	S(11 downto 0)		<= instruction(31 downto 25)	& 
				   instruction(11 downto 7);

	SB(31 downto 12)	<= (others => instruction(31));
	SB(11 downto 0)		<= instruction(31)		&
				   instruction(7)		&
				   instruction(30 downto 25)	&
				   instruction(11 downto 8);

	U(31 downto 20)		<= (others => instruction(31));
	U(19 downto 0)		<= instruction(31 downto 12);

	UJ(31 downto 20)	<= (others => instruction(31));
	UJ(19 downto 0)		<= instruction(31)		&
				   instruction(19 downto 12)	&
				   instruction(20)		&
				   instruction(30 downto 21);

	I_U_MUX : mux_2to1
		port map(
			-- input signals
			sel	=> sel1,
			in0	=> signed(I),
			in1	=> signed(U),

			-- output signal
			mux_out	=> I_or_U	
		);



	S_U_MUX : mux_2to1
		port map(
			-- input signals
			sel	=> sel1,
			in0	=> signed(S),
			in1	=> signed(U),

			-- output signals
			mux_out	=> S_or_U	
		);

	IMMEDIATE_MUX : mux4to1
		port map(
			-- input signals
			sel	=> sel2,
			in0	=> I_or_U,
			in1	=> S_or_U,
			in2	=> signed(UJ),
			in3	=> signed(SB_or_UJ),

			-- output signal
			muxOut	=> immediate		
		);
		
	UJ_SB_MUX : mux_2to1
		port map(
			-- input signals
			sel	=> sel1,
			in0	=> signed(SB),
			in1	=> signed(UJ),

			-- output signals
			mux_out	=> SB_or_UJ
		);

end architecture behaviour;
