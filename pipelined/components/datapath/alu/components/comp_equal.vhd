library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


entity comp_equal is
port(
	-- Inputs from register file
	rs1	: in signed(31 downto 0);
	rs2	: in signed(31 downto 0);
	-- Uscita
	zero	: out std_logic
);
end entity;

-- This comparator is described in a behavioural way, it returns 1 when the inputs are equal, 0 in the other cases 

architecture behaviour of comp_equal is

begin
	comparation : process(rs1,rs2)
	begin
		if rs1=rs2 then
			zero <= '1';
		else
			zero <= '0';
		end if;
	end process comparation;
end architecture behaviour;
