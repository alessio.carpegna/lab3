library ieee;
library std;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use std.textio.all;
use ieee.std_logic_textio.all;

entity srai_tb is
end entity srai_tb;


architecture test of srai_tb is

	component srai is
		generic(
			shamtLength	: integer := 5
				
		);
		port(
			-- Value to shift
			rs1		: in signed(2**shamtLength-1 downto 0);
			-- Shift amount (shamt)
			imm		: in signed(shamtLength-1 downto 0);
			
			-- Output signals
			sraiOut	: out signed(2**shamtLength-1 downto 0)	
		);
	end component srai;

	signal rs1	: signed(31 downto 0);
	signal imm	: signed(4 downto 0);
	signal sraiOut	: signed(31 downto 0);

 	file outFile	: text open write_mode is "srai_results.txt";

begin


	testing : process

		variable outLine	: line;

	begin
		rs1 <= to_signed(-2**31, 32);
		imm <= to_signed(0, 5);
	
		wait for 10 ns;

		write(outLine, std_logic_vector(rs1), right, 32);
		writeline(outFile, outLine);	
		write(outLine, to_integer(imm));
		writeline(outFile, outLine);	
		write(outLine, std_logic_vector(sraiOut), right, 32);
		writeline(outFile, outLine);	
		
		wait;
	end process testing;


	DUT : srai
		generic map(
			shamtLength 	=> 5
		)
		port map(
			rs1	=> rs1,
			imm	=> imm,
			sraiOut	=> sraiOut
		);

end architecture test;
