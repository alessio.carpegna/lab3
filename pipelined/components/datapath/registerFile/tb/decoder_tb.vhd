library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use std.textio.all;
use ieee.std_logic_textio.all;

entity decoder_tb is
end entity decoder_tb;

architecture test of decoder_tb is

	 component decoder is
	 	generic(
			N_inputs	: integer := 5		
		);
		port(
			-- input signal
			encodedIn	: in std_logic_vector(4 downto 0);

			-- output signal
			decodedOut	: out std_logic_vector(31 downto 0)
		);
	end component decoder;

	signal encodedIn	: std_logic_vector(4 downto 0);
	signal decodedOut	: std_logic_vector(31 downto 0);

	file outFile		: text open write_mode is "decoder_results.txt";
begin


	testing : process

		variable outLine	: line;

	begin
		for i in 0 to 31 loop
			encodedIn <= std_logic_vector(to_unsigned(i,5));
			wait for 10 ns;
			write(outLine, decodedOut, right, 32);
			writeline(outFile, outLine);
		end loop;
		wait;
	end process testing;

	DUT : decoder
		generic map(
			N_inputs	=> 5		
		)
		port map(
			-- input signal
			encodedIn	=> encodedIn,
			-- output signal
			decodedOut	=> decodedOut		
		);

end architecture test;
