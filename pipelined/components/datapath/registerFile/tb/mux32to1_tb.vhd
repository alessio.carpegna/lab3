library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use std.textio.all;
use ieee.std_logic_textio.all;

entity mux32to1_tb is
end entity mux32to1_tb;

architecture test of mux32to1_tb is

	component mux32to1 is
		generic(
			-- inputs parallelism
			L_inputs	: integer := 32		
		);
		port(
			-- input signals
			sel		: in std_logic_vector(4 downto 0);
			muxIn0		: in signed(L_inputs-1 downto 0);
			muxIn1		: in signed(L_inputs-1 downto 0);
			muxIn2		: in signed(L_inputs-1 downto 0);
			muxIn3		: in signed(L_inputs-1 downto 0);
			muxIn4		: in signed(L_inputs-1 downto 0);
			muxIn5		: in signed(L_inputs-1 downto 0);
			muxIn6		: in signed(L_inputs-1 downto 0);
			muxIn7		: in signed(L_inputs-1 downto 0);
			muxIn8		: in signed(L_inputs-1 downto 0);
			muxIn9		: in signed(L_inputs-1 downto 0);
			muxIn10		: in signed(L_inputs-1 downto 0);
			muxIn11		: in signed(L_inputs-1 downto 0);
			muxIn12		: in signed(L_inputs-1 downto 0);
			muxIn13		: in signed(L_inputs-1 downto 0);
			muxIn14		: in signed(L_inputs-1 downto 0);
			muxIn15		: in signed(L_inputs-1 downto 0);
			muxIn16		: in signed(L_inputs-1 downto 0);
			muxIn17		: in signed(L_inputs-1 downto 0);
			muxIn18		: in signed(L_inputs-1 downto 0);
			muxIn19		: in signed(L_inputs-1 downto 0);
			muxIn20		: in signed(L_inputs-1 downto 0);
			muxIn21		: in signed(L_inputs-1 downto 0);
			muxIn22		: in signed(L_inputs-1 downto 0);
			muxIn23		: in signed(L_inputs-1 downto 0);
			muxIn24		: in signed(L_inputs-1 downto 0);
			muxIn25		: in signed(L_inputs-1 downto 0);
			muxIn26		: in signed(L_inputs-1 downto 0);
			muxIn27		: in signed(L_inputs-1 downto 0);
			muxIn28		: in signed(L_inputs-1 downto 0);
			muxIn29		: in signed(L_inputs-1 downto 0);
			muxIn30		: in signed(L_inputs-1 downto 0);
			muxIn31		: in signed(L_inputs-1 downto 0);

			--output signal
			muxOut		: out signed(L_inputs-1 downto 0)
		);
	end component mux32to1;

	constant L_inputs	: integer := 32;

	-- test input signals
	signal sel		: std_logic_vector(4 downto 0);
	signal muxIn0		: signed(L_inputs-1 downto 0);
	signal muxIn1		: signed(L_inputs-1 downto 0);
	signal muxIn2		: signed(L_inputs-1 downto 0);
	signal muxIn3		: signed(L_inputs-1 downto 0);
	signal muxIn4		: signed(L_inputs-1 downto 0);
	signal muxIn5		: signed(L_inputs-1 downto 0);
	signal muxIn6		: signed(L_inputs-1 downto 0);
	signal muxIn7		: signed(L_inputs-1 downto 0);
	signal muxIn8		: signed(L_inputs-1 downto 0);
	signal muxIn9		: signed(L_inputs-1 downto 0);
	signal muxIn10		: signed(L_inputs-1 downto 0);
	signal muxIn11		: signed(L_inputs-1 downto 0);
	signal muxIn12		: signed(L_inputs-1 downto 0);
	signal muxIn13		: signed(L_inputs-1 downto 0);
	signal muxIn14		: signed(L_inputs-1 downto 0);
	signal muxIn15		: signed(L_inputs-1 downto 0);
	signal muxIn16		: signed(L_inputs-1 downto 0);
	signal muxIn17		: signed(L_inputs-1 downto 0);
	signal muxIn18		: signed(L_inputs-1 downto 0);
	signal muxIn19		: signed(L_inputs-1 downto 0);
	signal muxIn20		: signed(L_inputs-1 downto 0);
	signal muxIn21		: signed(L_inputs-1 downto 0);
	signal muxIn22		: signed(L_inputs-1 downto 0);
	signal muxIn23		: signed(L_inputs-1 downto 0);
	signal muxIn24		: signed(L_inputs-1 downto 0);
	signal muxIn25		: signed(L_inputs-1 downto 0);
	signal muxIn26		: signed(L_inputs-1 downto 0);
	signal muxIn27		: signed(L_inputs-1 downto 0);
	signal muxIn28		: signed(L_inputs-1 downto 0);
	signal muxIn29		: signed(L_inputs-1 downto 0);
	signal muxIn30		: signed(L_inputs-1 downto 0);
	signal muxIn31		: signed(L_inputs-1 downto 0);

	-- test output signal
	signal muxOut		: signed(L_inputs-1 downto 0);

	file outFile		: text open write_mode is "mux32to1_results.txt";


begin



	test : process
	
		variable outLine	: line;

	begin

		muxIn0 <= signed(to_unsigned(0,L_inputs));
		muxIn1 <= signed(to_unsigned(1,L_inputs));
		muxIn2 <= signed(to_unsigned(2,L_inputs));
		muxIn3 <= signed(to_unsigned(3,L_inputs));
		muxIn4 <= signed(to_unsigned(4,L_inputs));
		muxIn5 <= signed(to_unsigned(5,L_inputs));
		muxIn6 <= signed(to_unsigned(6,L_inputs));
		muxIn7 <= signed(to_unsigned(7,L_inputs));
		muxIn8 <= signed(to_unsigned(8,L_inputs));
		muxIn9 <= signed(to_unsigned(9,L_inputs));
		muxIn10 <= signed(to_unsigned(10,L_inputs));
		muxIn11 <= signed(to_unsigned(11,L_inputs));
		muxIn12 <= signed(to_unsigned(12,L_inputs));
		muxIn13 <= signed(to_unsigned(13,L_inputs));
		muxIn14 <= signed(to_unsigned(14,L_inputs));
		muxIn15 <= signed(to_unsigned(15,L_inputs));
		muxIn16 <= signed(to_unsigned(16,L_inputs));
		muxIn17 <= signed(to_unsigned(17,L_inputs));
		muxIn18 <= signed(to_unsigned(18,L_inputs));
		muxIn19 <= signed(to_unsigned(19,L_inputs));
		muxIn20 <= signed(to_unsigned(20,L_inputs));
		muxIn21 <= signed(to_unsigned(21,L_inputs));
		muxIn22 <= signed(to_unsigned(22,L_inputs));
		muxIn23 <= signed(to_unsigned(23,L_inputs));
		muxIn24 <= signed(to_unsigned(24,L_inputs));
		muxIn25 <= signed(to_unsigned(25,L_inputs));
		muxIn26 <= signed(to_unsigned(26,L_inputs));
		muxIn27 <= signed(to_unsigned(27,L_inputs));
		muxIn28 <= signed(to_unsigned(28,L_inputs));
		muxIn29 <= signed(to_unsigned(29,L_inputs));
		muxIn30 <= signed(to_unsigned(30,L_inputs));
		muxIn31 <= signed(to_unsigned(31,L_inputs));


		for i in 0 to 31 loop
			sel <= std_logic_vector(to_unsigned(i,5));
			wait for 10 ns;
			write(outLine, std_logic_vector(muxOut), right, 32);
			writeline(outFile, outLine);
		end loop;

		muxIn31 <= signed(to_unsigned(1,L_inputs));
		wait for 10 ns;
		write(outLine, std_logic_vector(muxOut), right, 32);
		writeline(outFile, outLine);

		muxIn31 <= signed(to_unsigned(2,L_inputs));
		wait for 10 ns;
		write(outLine, std_logic_vector(muxOut), right, 32);
		writeline(outFile, outLine);

		wait;
	end process;

	DUT : mux32to1
		generic map(
			-- inputs parallelism
			L_inputs	=> 32	
		)

		port map(
			-- input signals
			sel 		=> sel,
			muxIn0		=> muxIn0,
			muxIn1		=> muxIn1,
			muxIn2		=> muxIn2,
			muxIn3		=> muxIn3,
			muxIn4		=> muxIn4,
			muxIn5		=> muxIn5,
			muxIn6		=> muxIn6,
			muxIn7		=> muxIn7,
			muxIn8		=> muxIn8,
			muxIn9		=> muxIn9,
			muxIn10		=> muxIn10,
			muxIn11		=> muxIn11,
			muxIn12		=> muxIn12,
			muxIn13		=> muxIn13,
			muxIn14		=> muxIn14,
			muxIn15		=> muxIn15,
			muxIn16		=> muxIn16,
			muxIn17		=> muxIn17,
			muxIn18		=> muxIn18,
			muxIn19		=> muxIn19,
			muxIn20		=> muxIn20,
			muxIn21		=> muxIn21,
			muxIn22		=> muxIn22,
			muxIn23		=> muxIn23,
			muxIn24		=> muxIn24,
			muxIn25		=> muxIn25,
			muxIn26		=> muxIn26,
			muxIn27		=> muxIn27,
			muxIn28		=> muxIn28,
			muxIn29		=> muxIn29,
			muxIn30		=> muxIn30,
			muxIn31		=> muxIn31,
			muxOut		=> muxOut

		);

end architecture test;
