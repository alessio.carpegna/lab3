library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity regBeh_tb is
end entity regBeh_tb;

architecture test of regBeh_tb is

	component regBeh is
		generic(
			-- parallelism of the register
			N	: integer := 32
		);
		port(
			-- input signals
			clk	: in std_logic;
			en	: in std_logic;
			regIn	: in signed(N-1 downto 0);

			-- output signals
			regOut	: out signed(N-1 downto 0)
		);
	end component regBeh;

	-- test input signals
	signal clk	: std_logic;
	signal en	: std_logic;
	signal regIn	: signed(7 downto 0);

	-- test output signals
	signal regOut	: signed(7 downto 0);

begin

	clkGen : process
	begin
		clk <= '0';
		wait for 10 ns;
		clk <= '1';
		wait for 10 ns;
	end process clkGen;

	enGen : process
	begin
		en <= '0';
		wait for 55 ns;
		en <= '1';
		wait for 150 ns;
		en <= '0';
		wait;
	end process enGen;


	testing : process
	begin
		regIn <= "00000000";
		wait for 20 ns;
		regIn <= "11111111";
		wait for 45 ns;
		regIn <= "01010101";
		wait for 200 ns;
		regIn <= "00110011";
		wait;
	end process testing;


	sampling : process(clk)
	begin
		if clk'event and clk='1'
		then
			report "regIn: " & integer'image(to_integer(regIn)) & "    " 
			& "regOut: " & integer'image(to_integer(regOut));
		end if;

	end process sampling;


	DUT	: regBeh
		generic map(
			-- parallelism of the register
			N	=> 8	
		)
		port map(
			-- input signals
			clk	=> clk,
			en	=> en,
			regIn	=> regIn,

			-- output signals
			regOut	=> regOut
		);

end architecture test;
