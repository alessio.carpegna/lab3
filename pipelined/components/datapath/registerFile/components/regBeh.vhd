library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity regBeh is
	generic(
		-- parallelism of the register
		N	: integer := 32
	);
	port(
		-- input signals
		clk	: in std_logic;
		en	: in std_logic;
		regIn	: in signed(N-1 downto 0);

		-- output signals
		regOut	: out signed(N-1 downto 0)
	);
end entity regBeh;

architecture behaviour of regBeh is
begin

	sample : process(clk, en) is
	begin
		-- rising edge
		if clk'event and clk='1'
		then
			if en='1'
			then
				-- sample the input
				regOut <= regIn;
			end if;
		end if;
	end process sample;

end architecture behaviour;
