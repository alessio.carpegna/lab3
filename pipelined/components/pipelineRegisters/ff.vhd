library ieee;
use ieee.std_logic_1164.all;

entity ff is
port(
	clk	: in std_logic;
	d_in	: in std_logic;
	rst	: in std_logic;
	
	d_out	: out std_logic
);
end entity ff;

architecture bhv of ff is

begin

	process(clk, rst)
	begin

	if rst = '0' then
		d_out <= '0';
	elsif clk 'event and clk = '1' then
		d_out <= d_in;
	end if;
	end process;

end bhv;
