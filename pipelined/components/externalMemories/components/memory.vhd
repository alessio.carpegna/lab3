library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity memory is
	generic(
		startingAddress	: integer := 255;
		endAddress	: integer := 355;
		parallelism	: integer := 32		
	);
	port(
		-- input signals
		address		: in std_logic_vector(parallelism-3 downto 0);
		writeData	: in std_logic_vector(parallelism-1 downto 0);
		memRead		: in std_logic;
		memWrite	: in std_logic;

		-- output signals
		readData	: out std_logic_vector(parallelism-1 downto 0)
	);
end entity memory;

architecture behaviour of memory is

	-- memory array: it is addressed in byte and returns a word of 4 bytes.
	-- To avoid errors and to allow the processor to address all the
	-- possible bytes the memory is realized with 2^(addressSpace)+3 bytes.
	-- In this way if the the last addressable byte, that is the number
	-- 2^(addressSpace)-1, is choosen the memory is still able to return a
	-- 32 bits word, composed by the bytes in positions:
	--	2^(addressSpace-1) &
	--	2^(addressSpace) &
	--	2^(addressSpace+1) &
	--	2^(addressSpace+2)
	type memArray is array(startingAddress to endAddress) 
		of std_logic_vector(31 downto 0);
	signal mem2to32x32	: memArray;

begin

	readWrite : process(memRead, memWrite, address, writeData)
	begin

		if to_integer(unsigned(address))>=startingAddress and 
			to_integer(unsigned(address))<=endAddress
		then
			-- read: return a word made of 4 subsequent bytes
			if memRead = '1'
			then
				readData <=
					mem2to32x32(to_integer(unsigned(address)));

			-- write: write the word into 4 subsequent bytes
			elsif memWrite = '1'
			then
				mem2to32x32(to_integer(unsigned(address))) <= 	
					writeData;

			end if;
		else
			readData <= (others=>'0');
		end if;
	end process readWrite;

end architecture behaviour;
