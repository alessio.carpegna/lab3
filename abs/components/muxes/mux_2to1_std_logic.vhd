library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity mux_2to1_std_logic is 

	port(
		sel		: in std_logic;
		in0		: in std_logic_vector(31 downto 0);
		in1		: in std_logic_vector(31 downto 0);
		
		mux_out	: out std_logic_vector(31 downto 0)
	);
end entity mux_2to1_std_logic;

architecture structure of mux_2to1_std_logic is 

begin
	
	selection: process(sel, in0, in1)
	begin
		if sel = '0' then
			mux_out	<= in0;
		elsif sel = '1' then
			mux_out	<= in1;
		end if;
	end process selection;

end architecture structure;