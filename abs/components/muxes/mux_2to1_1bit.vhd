library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity mux_2to1_1bit is 

	port(
		sel		: in std_logic;
		in0		: in std_logic;
		in1		: in std_logic;
		
		mux_out	: out std_logic
	);
end entity mux_2to1_1bit;

architecture structure of mux_2to1_1bit is 

begin
	
	selection: process(sel, in0, in1)
	begin
		if sel = '0' then
			mux_out	<= in0;
		elsif sel = '1' then
			mux_out	<= in1;
		end if;
	end process selection;

end architecture structure;