library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity register_32bit_rst is
	port(
		--input signals
		clk			: in std_logic;
		reg_en		: in std_logic;
		rst			: in std_logic;
		d_in		: in signed(31 downto 0);

		--output signals
		d_out		: out signed(31 downto 0)
	);
end entity register_32bit_rst;

architecture behaviour of register_32bit_rst is
begin

	process(clk, rst)
	begin
		if(rst = '0') then
				d_out <= (others => '0');
		elsif(clk' event and clk='1') then
			if(reg_en = '1') then
				d_out <= d_in;	
			end if;
		end if;
	end process;
	
end architecture behaviour;
