set DUT "../components/srai.vhd"
set TB_DIR "../tb/"
set TB "srai_tb"

vcom $DUT
vcom $TB_DIR/$TB.vhd

vsim -t ns -novopt work.$TB
run 1 us

quit -f
