library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity adder_32bit is 

	port(
		add1	: in signed(31 downto 0);
		add2	: in signed(31 downto 0);
		
		sum_out	: out signed(31 downto 0)
	);
end entity adder_32bit;

architecture behavioral of adder_32bit is 

begin
	sum_out <= add1 + add2;

end architecture behavioral;