-- This component takes the 20 LSB and put them at the MSB of the output
-- It fills the remaining 12 bits with zeros

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity lui is 

	port(
		imm		: in signed(31 downto 0);
		
		lui_out	: out signed(31 downto 0)
	);
end entity lui;

architecture structure of lui is 

begin
	lui_out(31 downto 12) 	<= imm(19 downto 0);
	lui_out(11 downto 0)	<= (others => '0');

end architecture structure;