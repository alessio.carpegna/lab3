library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity comp_lower is
port(
	-- Ingressi
	rs1,rs2		: in signed(31 downto 0);
	-- Uscita
	comp_lower_out	: out signed(31 downto 0)
		
);
end entity comp_lower;

-- This component compares rs1 and rs2 and if rs1 is lower than rs2 it return the value 1 on 32 bits else the value 0 on 32 bits

architecture behaviour of comp_lower is

signal rs1_int,rs2_int : integer;

begin
	rs1_int <= to_integer(rs1);
	rs2_int <= to_integer(rs2);
	comparation : process(rs1_int,rs2_int)
	begin
		if rs1 < rs2 then
			comp_lower_out(31 downto 1) <= (others => '0');
			comp_lower_out(0)<= '1';	
		else
			comp_lower_out(31 downto 0) <= (others => '0');	
		end if;
	end process comparation;

end architecture behaviour;
