library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity andi is 

	port(
		rs1		: in signed(31 downto 0);
		imm		: in signed(31 downto 0);
		
		andi_out	: out signed(31 downto 0)
	);
end entity andi;

architecture structure of andi is 

begin
	andi_out <= rs1 AND imm;

end architecture structure;