set DUT "../components/regBeh.vhd"
set TB "regBeh_tb"
set TB_DIR "../tb"

vcom $DUT
vcom $TB_DIR/$TB.vhd

vsim -t ns -novopt work.$TB
run 300ns

quit -f
