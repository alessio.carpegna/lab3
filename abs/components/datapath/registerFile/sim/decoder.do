set DUT "../components/decoder.vhd"
set TB "decoder_tb"
set TB_DIR "../tb"

vcom $DUT
vcom $TB_DIR/$TB.vhd

vsim -t ns -novopt work.$TB
run 1us

quit -f
