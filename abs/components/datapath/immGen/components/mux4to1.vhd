library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity mux4to1 is
	generic(
		parallelism	: integer := 32	
	);
	port(
		-- input signals
		sel		: in std_logic_vector(1 downto 0);
		in0		: in signed(parallelism-1 downto 0);	
		in1		: in signed(parallelism-1 downto 0);		
		in2		: in signed(parallelism-1 downto 0);		
		in3		: in signed(parallelism-1 downto 0);	

		-- output signals
		muxOut		: out signed(parallelism-1 downto 0)	
	);
end entity mux4to1;

architecture behaviour of mux4to1 is
begin

	with sel select muxOut <=
		in0	when "00",
		in1	when "01",
		in2	when "10",
		in3	when others;

end architecture behaviour;
