library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity datapath is 

	port(
	
		clk		: in std_logic;
		rst		: in std_logic;
		
		IM_instruction	: in std_logic_vector(31 downto 0);	-- From Instruction of instruction mem to uP
		read_data	: in std_logic_vector(31 downto 0);	-- From read data of data mem to "memToReg" MUX of uP
		ALU_operation	: in std_logic_vector(3 downto 0);	-- From CU
		ALU_src1	: in std_logic;				-- From CU
		ALU_src2	: in std_logic;				-- From CU
		regWrite	: in std_logic;				-- From CU
		memToReg	: in std_logic;				-- From CU
		branch		: in std_logic;				-- From CU
		jal		: in std_logic;				-- From CU: OR with the out of the and gate to select the mux PC_SRC 
		
		pc		: out std_logic_vector(31 downto 0); 	-- From PC of uP to read address of instruction mem
		ALU_result	: out std_logic_vector(31 downto 0); 	-- From ALU result of uP to address of data memory
		read_data2	: out std_logic_vector(31 downto 0); 	-- From register file generator of uP to Write Data of data memory

		-- per testbench
		tb_mux_pc_src_sel	: out std_logic;
		tb_mux_pc_src		: out signed(31 downto 0);
		tb_b3_zero_out		: out std_logic;
		tb_immediate		: out signed(31 downto 0);
		tb_b3_jump_out		: out signed(31 downto 0);
		tb_rs1_pc		: out signed(31 downto 0);
		tb_rs2_imm		: out signed(31 downto 0)
	);
end entity datapath;

architecture behavioral of datapath is 

	component alu
		port(
			alu_operation	: in signed(3 downto 0);
			rs1		: in signed(31 downto 0);
			rs2		: in signed(31 downto 0);
			
			rd		: out signed(31 downto 0);
			zero		: out std_logic
		);
	end component alu;
	
	component regFile32_1w_2r
		generic(regParallelism	: integer := 32);
		port(
			-- input signals
			writeAddress	: in std_logic_vector(4 downto 0);
			readAddress1	: in std_logic_vector(4 downto 0);
			readAddress2	: in std_logic_vector(4 downto 0);
			dataIn		: in signed(regParallelism-1 downto 0);
			writeEn		: in std_logic;
			clk		: in std_logic;

			-- output signals
			readReg1	: out signed(regParallelism-1 downto 0);
			readReg2	: out signed(regParallelism-1 downto 0)
		);
	end component regFile32_1w_2r;

	component adder_32bit
		port(
			add1	: in signed(31 downto 0);
			add2	: in signed(31 downto 0);
			
			sum_out	: out signed(31 downto 0)
		);
	end component adder_32bit;
	
	component mux_2to1
		port(
			sel		: in std_logic;
			in0		: in signed(31 downto 0);
			in1		: in signed(31 downto 0);
			
			mux_out	: out signed(31 downto 0)
		);
	end component mux_2to1;
	
	component immGen
		generic(parallelism	: integer := 32);
		port(
			-- input signal
			instruction	: in std_logic_vector(parallelism-1 downto 0);

			-- output signal
			immediate	: out signed(parallelism-1 downto 0)
		);
	end component immGen;
	
	component register_32bit_rst	
	port(
		--input signals
		clk		: in std_logic;
		reg_en		: in std_logic;
		rst		: in std_logic;
		d_in		: in signed(31 downto 0);

		--output signals
		d_out		: out signed(31 downto 0)
	);
	end component register_32bit_rst;


	component reg_rst_std_logic is
		generic(
			parallelism	: integer := 32		
		);
		port(
			--input signals
			clk		: in std_logic;
			reg_en		: in std_logic;
			rst		: in std_logic;
			d_in		: in std_logic_vector(parallelism-1 downto 0);

			--output signals
			d_out		: out std_logic_vector(parallelism-1 downto 0)
		);
	end component reg_rst_std_logic;

	component ff is
	port(
		clk	: in std_logic;
		d_in	: in std_logic;
		rst	: in std_logic;
		
		d_out	: out std_logic
	);
	end component ff;



	--------------- SIGNALS ---------------
	
	signal read_data1_int, read_data2_int, alu_result_int, pc_out, pc_seq, pc_jump, immediate : signed(31 downto 0);
	
	signal mux_alu_src1, mux_alu_src2 , mux_memToReg, mux_PCsrc: signed(31 downto 0);
	
	signal zero_int, mux_PCsrc_sel : std_logic;
	signal shifted_imm: signed(31 downto 0);
	
	---------------------------------------

-- pipeline signals

	-- bank1
	signal b1_instr_out	: std_logic_vector(31 downto 0);
	signal b1_pc_out	: signed(31 downto 0);

	-- bank2
	signal b2_rs1_out	: signed(31 downto 0);
	signal b2_rs2_out	: signed(31 downto 0);
	signal b2_immGen_out	: signed(31 downto 0);
	signal b2_rd_out	: std_logic_vector(4 downto 0);
	signal b2_pc_out	: signed(31 downto 0);

	-- bank3
	signal b3_alu_out	: signed(31 downto 0);
	signal b3_zero_out	: std_logic;
	signal b3_rd_out	: std_logic_vector(4 downto 0);
	signal b3_rs2_out	: signed(31 downto 0);
	signal b3_jump_out	: signed(31 downto 0);

	-- bank4
	signal b4_alu_out	: signed(31 downto 0);
	signal b4_readData_out	: std_logic_vector(31 downto 0);
	signal b4_rd_out	: std_logic_vector(4 downto 0);



begin

	
	-- per testbench
	tb_mux_pc_src_sel	<= mux_PCsrc_sel;
	tb_mux_pc_src		<= mux_PCsrc;
	tb_b3_zero_out		<= b3_zero_out;
	tb_immediate		<= immediate;
	tb_b3_jump_out		<= b3_jump_out;
	tb_rs1_pc		<= mux_alu_src1;
	tb_rs2_imm	 	<= mux_alu_src2;





-- FETCH

	MUX_PC_SRC: mux_2to1 port map(
		sel	=> mux_PCsrc_sel, 
		in0	=> pc_seq,
		in1	=> b3_jump_out,
		
		mux_out	=> mux_PCsrc
	);


	PROGRAM_COUNTER: register_32bit_rst port map(
		clk	=> clk,
		reg_en	=> '1',
		rst	=> rst,
		d_in	=> mux_PCsrc,
		
		d_out	=> pc_out
	);


	ADDER_PC: adder_32bit port map(
		add1	=> pc_out,
		add2	=> "00000000000000000000000000000100",
		sum_out	=> pc_seq
	);

	pc		<= std_logic_vector(pc_out);



	bank1_instr : reg_rst_std_logic
		generic map(
			parallelism	=> 32
		)
		port map(
			--input signals
			clk		=> clk,	   
			reg_en	   	=> '1',
			rst	   	=> rst,
			d_in	   	=> IM_instruction,

			--output signals
			d_out	   	=> b1_instr_out
		);

	bank1_pc : register_32bit_rst	
		port map(
			--input signals
			clk		=> clk,	
			reg_en		=> '1',
			rst		=> rst,
			d_in		=> pc_out,

			--output signals
			d_out		=> b1_pc_out
		);


-- DECODE

	REGISTER_FILE: regFile32_1w_2r generic map(regParallelism => 32)
	port map(
		clk		=> clk,
		writeAddress	=> b4_rd_out,
		readAddress1	=> b1_instr_out(19 downto 15),
		readAddress2	=> b1_instr_out(24 downto 20),
		dataIn		=> mux_memToReg,
		writeEn		=> regWrite,
		
		readReg1	=> read_data1_int,
		readReg2	=> read_data2_int
	);

	IMM_GENERATION: immGen port map(
		instruction	=> std_logic_vector(b1_instr_out),
		immediate	=> immediate
	);



	bank2_rs1 : register_32bit_rst	
		port map(
			--input signals
			clk		=> clk,	
			reg_en		=> '1',
			rst		=> rst,
			d_in		=> read_data1_int,

			--output signals
			d_out		=> b2_rs1_out
		);

	bank2_rs2 : register_32bit_rst	
		port map(
			--input signals
			clk		=> clk,	
			reg_en		=> '1',
			rst		=> rst,
			d_in		=> read_data2_int,

			--output signals
			d_out		=> b2_rs2_out
		);

	bank2_immGen : register_32bit_rst	
		port map(
			--input signals
			clk		=> clk,	
			reg_en		=> '1',
			rst		=> rst,
			d_in		=> immediate,

			--output signals
			d_out		=> b2_immGen_out
		);

	bank2_rd : reg_rst_std_logic
		generic map(
			parallelism	=> 5
		)
		port map(
			--input signals
			clk		=> clk,	   
			reg_en	   	=> '1',
			rst	   	=> rst,
			d_in	   	=> b1_instr_out(11 downto 7),

			--output signals
			d_out	   	=> b2_rd_out
		);

	bank2_pc : register_32bit_rst	
		port map(
			--input signals
			clk		=> clk,	
			reg_en		=> '1',
			rst		=> rst,
			d_in		=> b1_pc_out,

			--output signals
			d_out		=> b2_pc_out
		);


-- EXECUTE
	MUX_ALU_PC_DATA1: mux_2to1 port map(
		sel	=> ALU_src1, 
		in0	=> b2_pc_out,
		in1	=> b2_rs1_out,
		
		mux_out	=> mux_alu_src1
	);
	
	MUX_ALU_IMM_DATA2: mux_2to1 port map(
		sel	=> ALU_src2, 
		in0	=> b2_rs2_out,
		in1	=> b2_immGen_out,
		
		mux_out	=> mux_alu_src2
	);



	ALU_BLOCK: alu port map(
		
		alu_operation	=> signed(ALU_operation),
		rs1		=> mux_alu_src1,
		rs2		=> mux_alu_src2,
		
		rd		=> alu_result_int,
		zero		=> zero_int		
	);
	

	shifted_imm(0)			<= '0';
	shifted_imm(31 downto 1) 	<= b2_immGen_out(30 downto 0);


	ADDER_JUMP: adder_32bit port map(
		add1	=> b2_pc_out,
		add2	=> shifted_imm,
		
		sum_out	=> pc_jump
	);




	bank3_alu : register_32bit_rst	
		port map(
			--input signals
			clk		=> clk,	
			reg_en		=> '1',
			rst		=> rst,
			d_in		=> alu_result_int,

			--output signals
			d_out		=> b3_alu_out
		);


	ALU_result	<= std_logic_vector(b3_alu_out);


	bank3_zero : ff
		port map(
			-- input signals
			clk		=> clk,	
			d_in		=> zero_int,
			rst		=> rst,
			
			-- output signals
			d_out		=> b3_zero_out
		);


	bank3_rd : reg_rst_std_logic
		generic map(
			parallelism	=> 5
		)
		port map(
			--input signals
			clk		=> clk,	   
			reg_en	   	=> '1',
			rst	   	=> rst,
			d_in	   	=> b2_rd_out,

			--output signals
			d_out	   	=> b3_rd_out
		);

	bank3_rs2 : register_32bit_rst	
		port map(
			--input signals
			clk		=> clk,	
			reg_en		=> '1',
			rst		=> rst,
			d_in		=> b2_rs2_out,

			--output signals
			d_out		=> b3_rs2_out
		);


	read_data2	<= std_logic_vector(b3_rs2_out);


	bank3_jump : register_32bit_rst	
		port map(
			--input signals
			clk		=> clk,	
			reg_en		=> '1',
			rst		=> rst,
			d_in		=> pc_jump,

			--output signals
			d_out		=> b3_jump_out
		);



-- MEM

	mux_PCsrc_sel			<= jal OR (b3_zero_out AND branch);


	bank4_alu : register_32bit_rst	
		port map(
			--input signals
			clk		=> clk,	
			reg_en		=> '1',
			rst		=> rst,
			d_in		=> b3_alu_out,

			--output signals
			d_out		=> b4_alu_out
		);

	bank4_readData : reg_rst_std_logic
		generic map(
			parallelism	=> 32		
		)	
		port map(
			--input signals
			clk		=> clk,	
			reg_en		=> '1',
			rst		=> rst,
			d_in		=> read_data,

			--output signals
			d_out		=> b4_readData_out
		);



	bank4_rd : reg_rst_std_logic
		generic map(
			parallelism	=> 5
		)
		port map(
			--input signals
			clk		=> clk,	   
			reg_en	   	=> '1',
			rst	   	=> rst,
			d_in	   	=> b3_rd_out,

			--output signals
			d_out	   	=> b4_rd_out
		);




	MUX_MEM_TO_REG: mux_2to1
		port map(
			sel		=> memToReg,
			in1		=> signed(b4_readData_out),
			in0		=> b4_alu_out,
		
			mux_out		=> mux_memToReg
		);
	

	
	
	
	

end architecture behavioral;
