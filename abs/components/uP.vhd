library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity uP is
	port(
		clk		: in std_logic;
		rst		: in std_logic;
		
		IM_instruction	: in std_logic_vector(31 downto 0);
		read_data	: in std_logic_vector(31 downto 0);

		ALU_result	: out std_logic_vector(31 downto 0);
		writeData	: out std_logic_vector(31 downto 0);
		PC		: out std_logic_vector(31 downto 0);	
		memWrite	: out std_logic;
		memRead		: out std_logic;
		
		--- per tb ---
		tb_CU_instructions: out std_logic_vector(11 downto 0);
		--------------

		-- per testbench
		tb_mux_pc_src_sel	: out std_logic;
		tb_mux_pc_src		: out signed(31 downto 0);
		tb_b3_zero_out		: out std_logic;
		tb_immediate		: out signed(31 downto 0);
		tb_b3_jump_out		: out signed(31 downto 0);
		tb_b3_branch_out	: out std_logic;
		tb_rs1_pc		: out signed(31 downto 0);
		tb_rs2_imm		: out signed(31 downto 0)



	);
end entity uP;

architecture behaviour of uP is

	component Datapath
		port(
			-- input signals
			clk		: in std_logic;
			rst		: in std_logic;
			
			IM_instruction	: in std_logic_vector(31 downto 0);  -- From Instruction of instruction mem to uP
			read_data	: in std_logic_vector(31 downto 0);  -- From read data of data mem to "memToReg" MUX of uP
			ALU_operation	: in std_logic_vector(3 downto 0);	 -- From CU
			ALU_src1	: in std_logic;			 -- From CU
			ALU_src2	: in std_logic;			 -- From CU
			regWrite	: in std_logic;			 -- From CU
			memToReg	: in std_logic;			 -- From CU
			branch		: in std_logic;			 -- From CU
			jal		: in std_logic;			 -- From CU: OR with the out of the
										 -- and gate to select the mux PC_SRC 
			-- output signals
			pc		: out std_logic_vector(31 downto 0); -- From PC of uP to read address of instruction mem
			ALU_result	: out std_logic_vector(31 downto 0); -- From ALU result of uP to address of data memory
			read_data2	: out std_logic_vector(31 downto 0);  -- From Immediate generator of uP
										     -- to Write Data of data memory

			-- per testbench
			tb_mux_pc_src_sel	: out std_logic;
			tb_mux_pc_src		: out signed(31 downto 0);
			tb_b3_zero_out		: out std_logic;
			tb_immediate		: out signed(31 downto 0);
			tb_b3_jump_out		: out signed(31 downto 0);
			tb_rs1_pc		: out signed(31 downto 0);
			tb_rs2_imm		: out signed(31 downto 0)


		);
	end component Datapath;
	
	component CU 
		port(
			-- input signals
			IM_instruction	: in std_logic_vector(31 downto 0);

			-- output signals
			ALU_operation	: out std_logic_vector(3 downto 0);	
			ALU_src1	: out std_logic;
			ALU_src2	: out std_logic;
			regWrite	: out std_logic;
			memToReg	: out std_logic;
			branch		: out std_logic;
			jal		: out std_logic;
			memRead		: out std_logic;
			memWrite	: out std_logic	
		);
	end component CU;

	component reg_rst_std_logic is
		generic(
			parallelism	: integer := 32		
		);
		port(
			--input signals
			clk		: in std_logic;
			reg_en		: in std_logic;
			rst		: in std_logic;
			d_in		: in std_logic_vector(parallelism-1 downto 0);

			--output signals
			d_out		: out std_logic_vector(parallelism-1 downto 0)
		);
	end component reg_rst_std_logic;


	component ff is
	port(
		clk	: in std_logic;
		d_in	: in std_logic;
		rst	: in std_logic;
		
		d_out	: out std_logic
	);
	end component ff;


	signal read_data_int		: std_logic_vector(31 downto 0);
	signal ALU_operation_int	: std_logic_vector(3 downto 0);
	signal ALU_src1_int		: std_logic;
	signal ALU_src2_int		: std_logic;
	signal regWrite_int		: std_logic;
	signal memToReg_int		: std_logic;
	signal branch_int		: std_logic;
	signal jal_int			: std_logic;

	--- per tb ---
	signal memRead_int, memWrite_int	: std_logic;
	--------------


	-- input control signals concatenated
	signal controlSignals	: std_logic_vector(11 downto 0);

	--bank1
	signal b1_instr_out	: std_logic_vector(31 downto 0);

	-- bank2
	signal b2_signals_out	: std_logic_vector(11 downto 0);

	-- bank3
	signal b3_signals_out	: std_logic_vector(5 downto 0);

	-- bank4
	signal b4_signals_out	: std_logic_vector(1 downto 0);

begin

	controlSignals		<= 	ALU_operation_int	&
					ALU_src1_int 		&
					ALU_src2_int 		&
					regWrite_int 		&
					memToReg_int		&
					branch_int		&
					jal_int			&
					memRead_int		&
					memWrite_int;



	memRead			<= b3_signals_out(1);
	memWrite		<= b3_signals_out(0);


	------- per tb -------
	tb_CU_instructions	<= controlSignals;
	tb_b3_branch_out	<= b3_signals_out(3);
	------------------------


	bank1_instr : reg_rst_std_logic
	generic map(
		parallelism	=> 32
	)
	port map(
		--input signals
		clk		=> clk,	   
		reg_en	   	=> '1',
		rst	   	=> rst,
		d_in	   	=> IM_instruction,

		--output signals
		d_out	   	=> b1_instr_out
	);



	CONTROL_UNIT: CU port map(
	
		IM_instruction	=> b1_instr_out,
		
		ALU_operation	=> ALU_operation_int,
		ALU_src1	=> ALU_src1_int,
		ALU_src2	=> ALU_src2_int,
		regWrite	=> regWrite_int,
		memToReg	=> memToReg_int,
		branch		=> branch_int,
		jal		=> jal_int,
		memRead		=> memRead_int,
		memWrite	=> memWrite_int
	);


	bank2_signals : reg_rst_std_logic
		generic map(
			parallelism	=> 12
		)
		port map(
			--input signals
			clk		=> clk,		
			reg_en		=> '1',
			rst		=> rst,
			d_in		=> controlSignals,

			--output signals
			d_out		=> b2_signals_out
		);

	

	bank3_signals : reg_rst_std_logic
		generic map(
			parallelism	=> 6
		)
		port map(
			--input signals
			clk		=> clk,		
			reg_en		=> '1',
			rst		=> rst,
			d_in		=> b2_signals_out(5 downto 0),

			--output signals
			d_out		=> b3_signals_out
		);

	bank4_signals : reg_rst_std_logic
		generic map(
			parallelism	=> 2
		)
		port map(
			--input signals
			clk		=> clk,		
			reg_en		=> '1',
			rst		=> rst,
			d_in		=> b3_signals_out(5 downto 4),

			--output signals
			d_out		=> b4_signals_out
		);

	




	DP:	Datapath port map(
	
		clk		=> clk,
		rst		=> rst,
		
		IM_instruction	=> IM_instruction,
		read_data	=> read_data,
		ALU_operation	=> b2_signals_out(11 downto 8),
		ALU_src1	=> b2_signals_out(7),
		ALU_src2	=> b2_signals_out(6),
		regWrite	=> b4_signals_out(1),
		memToReg	=> b4_signals_out(0),
		branch		=> b3_signals_out(3),
		jal		=> b3_signals_out(2),
		
		pc		=> pc,
		ALU_result	=> ALU_result,
		read_data2	=> writeData,

		-- per testbench
		tb_mux_pc_src_sel	=>	tb_mux_pc_src_sel,
		tb_mux_pc_src		=>	tb_mux_pc_src,
		tb_b3_zero_out		=>	tb_b3_zero_out,
		tb_immediate		=>	tb_immediate,
		tb_b3_jump_out		=>	tb_b3_jump_out,
		tb_rs1_pc		=>	tb_rs1_pc,
		tb_rs2_imm		=>	tb_rs2_imm


	);
	
end architecture behaviour;
