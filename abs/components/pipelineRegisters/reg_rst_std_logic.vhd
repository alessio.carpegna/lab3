library ieee;
use ieee.std_logic_1164.all;

entity reg_rst_std_logic is
	generic(
		parallelism	: integer := 32		
	);
	port(
		--input signals
		clk		: in std_logic;
		reg_en		: in std_logic;
		rst		: in std_logic;
		d_in		: in std_logic_vector(parallelism-1 downto 0);

		--output signals
		d_out		: out std_logic_vector(parallelism-1 downto 0)
	);
end entity reg_rst_std_logic;

architecture behaviour of reg_rst_std_logic is
begin

	process(clk, rst)
	begin
		if(rst = '0') 
		then		
			d_out <= (others => '0');
		elsif(clk' event and clk='1') 
		then
			if(reg_en = '1')
			then
				d_out <= d_in;	
			end if;
		end if;
	end process;
	
end architecture behaviour;
