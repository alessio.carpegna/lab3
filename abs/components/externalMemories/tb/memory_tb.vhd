library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use std.textio.all;
use ieee.std_logic_textio.all;

entity memory_tb is
end entity memory_tb;

architecture test of memory_tb is

	component memory is
		generic(
			addressSpace	: integer := 32;
			parallelism	: integer := 32		
		);
		port(
			-- input signals
			address		: in std_logic_vector(addressSpace-1 downto 0);
			writeData	: in std_logic_vector(parallelism-1 downto 0);
			memRead		: in std_logic;
			memWrite	: in std_logic;

			-- output signals
			readData	: out std_logic_vector(parallelism-1 downto 0)
		);
	end component memory;

	signal address		: std_logic_vector(31 downto 0);
	signal writeData	: std_logic_vector(31 downto 0);
	signal memRead		: std_logic;
	signal memWrite		: std_logic;
	signal readData		: std_logic_vector(31 downto 0);

	file outFile		: text open write_mode is "memory_results.txt";


begin


	testing : process

		variable outLine	: line;
		variable j		: integer := 0;

	begin

		-- write in the first four words
		memWrite		<= '1';
		memRead			<= '0';
		j := 0;
		write4words: for i in 0 to 3
		loop
			address		<= std_logic_vector(to_unsigned(j,32));
			writeData	<= std_logic_vector(to_unsigned(j+4,32));
			j := j + 4;
			wait for 10 ns;
			write(outLine, readData, right, 32);
			writeline(outFile, outline);
		end loop write4words;



		-- read the first four words with memWrite='0'
		memWrite		<= '0';
		memRead			<= '1';
		j := 0;
		read4words0: for i in 0 to 3
		loop
			address		<= std_logic_vector(to_unsigned(j,32));
			j := j + 4;
			wait for 10 ns;
			write(outLine, readData, right, 32);
			writeline(outFile, outline);
		end loop read4words0;


		-- read the first four words with memWrite='1'
		memWrite		<= '1';
		memRead			<= '1';
		j := 0;
		read4words1: for i in 0 to 3
		loop
			address		<= std_logic_vector(to_unsigned(j,32));
			j := j + 4;
			wait for 10 ns;
			write(outLine, readData, right, 32);
			writeline(outFile, outline);
		end loop read4words1;
		
		-- try to use the memory with memRead and memWrite to '0' 
		memWrite		<= '0';
		memRead			<= '0';
		j := 0;
		try00 : for i in 0 to 3
		loop
			address		<= std_logic_vector(to_unsigned(j,32));
			j := j + 4;
			wait for 10 ns;
			write(outLine, readData, right, 32);
			writeline(outFile, outline);
		end loop try00;


		-- try to write with memRead='1'
		memWrite		<= '1';
		memRead			<= '1';
		j := 0;
		write4words1: for i in 0 to 3
		loop
			address		<= std_logic_vector(to_unsigned(j,32));
			writeData	<= std_logic_vector(to_unsigned(j+5,32));
			j := j + 4;
			wait for 10 ns;
			write(outLine, readData, right, 32);
			writeline(outFile, outline);
		end loop write4words1;


		-- read the first four words with memWrite='1'
		memWrite		<= '1';
		memRead			<= '1';
		j := 0;
		read4words1_2: for i in 0 to 3
		loop
			address		<= std_logic_vector(to_unsigned(j,32));
			j := j + 4;
			wait for 10 ns;
			write(outLine, readData, right, 32);
			writeline(outFile, outline);
		end loop read4words1_2;



		wait;

	end process testing;


	DUT : memory
		generic map(
			addressSpace	=> 32,
			parallelism	=> 32	
		)
		port map(
			-- input signals
			address		=> address,
			writeData	=> writeData,
			memRead		=> memRead,
			memWrite	=> memWrite,

			-- output signals
			readData	=> readData	
		);


end architecture test;
