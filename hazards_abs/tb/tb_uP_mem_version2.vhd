library ieee;
use ieee.std_logic_1164.all;
use std.textio.all;
use ieee.std_logic_textio.all;
use ieee.numeric_std.all;


entity tb_uP_mem_version2 is
end entity tb_uP_mem_version2;

architecture test of tb_uP_mem_version2 is


-- components

	component memory is
		generic(
			startingAddress	: integer := 0;
			endAddress	: integer := 8;
			parallelism	: integer := 32		
		);
		port(
			-- input signals
			address		: in std_logic_vector(parallelism-3 downto 0);
			writeData	: in std_logic_vector(parallelism-1 downto 0);
			memRead		: in std_logic;
			memWrite	: in std_logic;

			-- output signals
			readData	: out std_logic_vector(parallelism-1 downto 0)
		);
	end component memory;



	component mux_2to1_std_logic is 
		port(
			-- input signals
			sel		: in std_logic;
			in0		: in std_logic_vector(31 downto 0);
			in1		: in std_logic_vector(31 downto 0);
			
			-- output signals
			mux_out	: out std_logic_vector(31 downto 0)
		);
	end component mux_2to1_std_logic;


	component mux_2to1_1bit is 
		port(
			-- input signals
			sel		: in std_logic;
			in0		: in std_logic;
			in1		: in std_logic;

			-- output signals
			mux_out	: out std_logic
		);
	end component mux_2to1_1bit;


	component uP is
		port(
			-- input signals
			clk			: in std_logic;
			rst			: in std_logic;
			
			IM_instruction		: in std_logic_vector(31 downto 0);
			read_data		: in std_logic_vector(31 downto 0);

			-- output signals	 
			ALU_result		: out std_logic_vector(31 downto 0);
			writeData		: out std_logic_vector(31 downto 0);
			PC					: out std_logic_vector(31 downto 0);	
			memWrite		: out std_logic;
			memRead			: out std_logic
		);
	end component uP;


-- constant values

	-- internal parallelism of the whole system
	constant parallelism		: integer := 32;

	-- data memory addresses
	constant dataStart		: integer := 66076672;
	constant dataEnd		: integer := dataStart + 10;

	-- instruction memory addresses
	constant instrStart		: integer := 0;
	constant instrEnd		: integer := instrStart + 130;




-- signal controlled by the testbench

	-- uP
	signal clk			: std_logic;
	signal rst			: std_logic;

	-- data memory
	signal extData			: std_logic_vector(parallelism-1 downto 0);
	signal extAddressData		: std_logic_vector(parallelism-1 downto 0);
	signal extInstr			: std_logic_vector(parallelism-1 downto 0);
	signal extAddressInstr		: std_logic_vector(parallelism-1 downto 0);
	signal ext_memRead		: std_logic;
	signal ext_memWrite		: std_logic;

	-- instruction memory
	signal instrWrite		: std_logic_vector(31 downto 0);
	signal readInstr		: std_logic;
	signal loadInstr		: std_logic;

	-- selection muxes
	signal loadExt			: std_logic;



-- signals used to synchronize the various processes
	signal instrFileEnd		: std_logic := '0';
	signal dataFileEnd		: std_logic := '0';	


-- signals used to obtain incremental addresses for the two memories
	signal writeInstrAddress	: integer := 0;
	signal writeDataAddress		: integer := dataStart*4;


-- internal signals

	-- data memory
	signal writeData		: std_logic_vector(parallelism-1 downto 0);
	signal dataAddress		: std_logic_vector(parallelism-1 downto 0);
	signal memRead			: std_logic;
	signal memWrite			: std_logic;
	signal readData			: std_logic_vector(parallelism-1 downto 0);


	-- instruction memory
	signal instrAddress		: std_logic_vector(parallelism-1 downto 0);
	signal IM_instruction		: std_logic_vector(parallelism-1 downto 0);

	-- uP
	signal PC			: std_logic_vector(parallelism-1 downto 0);
	signal writeData_uP		: std_logic_vector(parallelism-1 downto 0);
	signal aluResult		: std_logic_vector(parallelism-1 downto 0);
	signal memRead_uP		: std_logic;
	signal memWrite_uP		: std_logic;


	-- files
	file instrFile 			: text open read_mode is "textSectionPipelined.txt";
	file dataFile 			: text open read_mode is  "dataSection.txt";
	file resultsFile		: text open write_mode is "results.txt";

	begin


	-- processes

	-- clock
	clkGen : process
	begin
		clk <= '0';
		wait for 10 ns;
		clk <= '1';
		wait for 10 ns;

	end process clkGen;


	-- data
	loadData : process(clk)
		
		variable inLine		: line;
		variable extData_var	: std_logic_vector(parallelism-1 downto 0);

	begin

		if clk'event and clk = '1'
		then
			if not endfile(dataFile)
			then
				-- read data
				readline(dataFile, inLine);
				read(inLine, extData_var);
				extData 		<= extData_var;

				extAddressData 		<= std_logic_vector(to_unsigned(writeDataAddress, 32));
				writeDataAddress 	<= writeDataAddress + 4;
				dataFileEnd	<= '0';
			else
				dataFileEnd 		<= '1';
			end if;
		end if;

	end process loadData;


	-- instructions
	loadInstructions : process(clk, dataFileEnd)
		
		variable inLine		: line;
		variable extInstr_var	: std_logic_vector(parallelism-1 downto 0);

	begin

		if clk'event and clk = '1'
		then
			if not endfile(instrFile)
			then
				-- read instruction
				readline(instrFile, inLine);
				read(inLine, extInstr_var);
				extInstr 		<= extInstr_var;

				extAddressInstr 	<= std_logic_vector(to_unsigned(writeInstrAddress, 32));
				writeInstrAddress 	<= writeInstrAddress + 4;
				instrFileEnd	<= '0';

			else
				instrFileEnd 		<= '1';	
			end if;
		end if;

	end process loadInstructions;


	-- drive signals
	driveSignals : process(clk, dataFileEnd, instrFileEnd)
	begin

		-- default values

		if clk'event and clk = '1'
		then
			-- end load data
			if dataFileEnd = '1'
			then
				-- end load instructions
				if instrFileEnd = '1'
				then
					rst		<= '1';
					loadExt		<= '0';
					loadInstr	<= '0';
					readInstr	<= '1';
					ext_memWrite	<= '0';
					ext_memRead	<= '0';

				-- continue to load instructions
				else

					rst		<= '0';
					loadExt		<= '1';
					loadInstr	<= '1';
					readInstr	<= '0';
					ext_memWrite	<= '0';
					ext_memRead	<= '0';
				end if;

			-- continue to load data
			else

				-- end load instruction
				if instrFileEnd = '1'
				then
					rst		<= '0';
					loadExt		<= '1';
					loadInstr	<= '0';
					readInstr	<= '0';
					ext_memWrite	<= '1';
					ext_memRead	<= '0';

				-- continue to load instructions
				else

					rst		<= '0';
					loadExt		<= '1';
					loadInstr	<= '1';
					readInstr	<= '0';
					ext_memWrite	<= '1';
					ext_memRead	<= '0';
				end if;
			end if;
		end if;
	end process driveSignals;


	writeOutput : process(memWrite_uP)
		
		variable outLine	: line;

	begin
		if memWrite_uP = '1'
		then
			write(outLine, to_integer(signed(writeData_uP)));
			writeline(resultsFile, outLine);
		end if;
		
	end process writeOutput;


-- port map

	instruction_memory : memory
		generic map(
			startingAddress	=> instrStart,
			endAddress	=> instrEnd,
			parallelism	=> parallelism	
		)
		port map(
				
			-- input signals
			address		=> instrAddress(31 downto 2),	
			writeData	=> instrWrite,
			memRead		=> readInstr,
			memWrite	=> loadInstr,

			-- output signal
			readData	=> IM_instruction
		);

	data_memory : memory
		generic map(
			startingAddress	=> dataStart,
			endAddress	=> dataEnd,
			parallelism	=> parallelism	
		)
		port map(
				
			-- input signals
			address		=> dataAddress(31 downto 2),	
			writeData	=> writeData,
			memRead		=> memRead,
			memWrite	=> memWrite,

			-- output signal
			readData	=> readData
		);

	processor : uP
		port map(
			-- input signals
			clk		=> clk,		
			rst		=> rst,	

			IM_instruction	=> IM_instruction,	
			read_data	=> readData,

			-- output signals	
			ALU_result	=> aluResult,
			writeData	=> writeData_uP,
			PC		=> PC,	
			memWrite	=> memWrite_uP,
			memRead		=> memRead_uP

		);

	address_instr_mem_mux : mux_2to1_std_logic
		port map(
			-- input signals
			sel		=> loadExt,		
			in0		=> PC,
			in1		=> extAddressInstr,
			
			-- output signal
			mux_out		=> instrAddress
			
		);

	memRead_dataMem_mux : mux_2to1_1bit
		port map(
			-- input signals
			sel		=> loadExt,	
			in0		=> memRead_uP,
			in1		=> ext_memRead,

			-- output signal
			mux_out		=> memRead
		);

	memWrite_dataMem_mux : mux_2to1_1bit
		port map(
			-- input signals
			sel		=> loadExt,	
			in0		=> memWrite_uP,
			in1		=> ext_memWrite,

			-- output signal
			mux_out		=> memWrite
		);

	data_dataMem_mux : mux_2to1_std_logic
		port map(
			-- input signals
			sel		=> loadExt,		
			in0		=> writeData_uP,
			in1		=> extData,
			
			-- output signal
			mux_out		=> writeData
			
		);

	address_dataMem_mux : mux_2to1_std_logic
		port map(
			-- input signals
			sel		=> loadExt,		
			in0		=> aluResult,
			in1		=> extAddressData,
			
			-- output signal
			mux_out		=> dataAddress
			
		);


	-- mux used to synchronize the address and the dataIn inputs
	-- of the instruction memory
	dummyMux : mux_2to1_std_logic 
		port map(
			-- input signals
			sel		=> loadExt,	
			in0		=> extInstr,
			in1		=> extInstr,
			
			-- outpu
			mux_out		=> instrWrite
		);




end architecture test;
