#!/usr/bin/bash

TCL_SCRIPT="$1"

# Set up the environment to properly run Modelsim
source /software/scripts/init_synopsys_64.18

# Execute the desired set of Modelsim commands
dc_shell-xg-t -f $TCL_SCRIPT

rm *.syn
rm *.mr
rm ARCH
rm ENTI
