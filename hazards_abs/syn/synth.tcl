# Internal variables
set TOP_ENTITY uP
set ARCH behaviour
set CLOCK_SIGNAL MY_CLK
set CLK_INPUT clk
set CLK_PERIOD 0.00
set CLK_UNCERT 0.07
set IN_DELAY 0.5
set OUT_DELAY 0.5

# Output files
set REPORT_TIMING report_timing.txt
set REPORT_AREA report_area.txt

# Import vhdl source files in the current design
# 	-f = format of the source files
analyze -f vhdl -lib WORK ../components/uP.vhd
analyze -f vhdl -lib WORK ../components/cu/components/CU.vhd
analyze -f vhdl -lib WORK ../components/datapath/datapath.vhd
analyze -f vhdl -lib WORK ../components/datapath/alu/components/abs.vhd
analyze -f vhdl -lib WORK ../components/datapath/alu/components/adder_32bit.vhd
analyze -f vhdl -lib WORK ../components/datapath/alu/components/alu.vhd
analyze -f vhdl -lib WORK ../components/datapath/alu/components/andi.vhd
analyze -f vhdl -lib WORK ../components/datapath/alu/components/comp_equal.vhd
analyze -f vhdl -lib WORK ../components/datapath/alu/components/comp_lower.vhd
analyze -f vhdl -lib WORK ../components/datapath/alu/components/lui.vhd
analyze -f vhdl -lib WORK ../components/datapath/alu/components/mux_2to1.vhd
analyze -f vhdl -lib WORK ../components/datapath/alu/components/mux_operation.vhd
analyze -f vhdl -lib WORK ../components/datapath/alu/components/srai.vhd
analyze -f vhdl -lib WORK ../components/datapath/alu/components/xor_block.vhd
analyze -f vhdl -lib WORK ../components/datapath/immGen/components/immGen.vhd
analyze -f vhdl -lib WORK ../components/datapath/immGen/components/mux4to1.vhd
analyze -f vhdl -lib WORK ../components/datapath/pc/register_32bit_rst.vhd
analyze -f vhdl -lib WORK ../components/datapath/registerFile/components/decoder.vhd
analyze -f vhdl -lib WORK ../components/datapath/registerFile/components/mux32to1.vhd
analyze -f vhdl -lib WORK ../components/datapath/registerFile/components/regBeh.vhd
analyze -f vhdl -lib WORK ../components/datapath/registerFile/components/regFile32_1w_2r.vhd
analyze -f vhdl -lib WORK ../components/externalMemories/components/memory.vhd
analyze -f vhdl -lib WORK ../components/hazards/forwarding_unit.vhd
analyze -f vhdl -lib WORK ../components/pipelineRegisters/ff.vhd
analyze -f vhdl -lib WORK ../components/pipelineRegisters/reg_rst_std_logic.vhd
analyze -f vhdl -lib WORK ../components/muxes/mux_2to1_1bit.vhd
analyze -f vhdl -lib WORK ../components/muxes/mux_2to1_std_logic.vhd



# Preserve RTL names in the generated netlist
set power_preserve_rtl_hier_names true

# Synthesize the desired architecture (-arch) of the main entity
elaborate $TOP_ENTITY -arch $ARCH -lib WORK

# Make the various instances of a component to refer to the same
# component
uniquify

link


create_clock -name $CLOCK_SIGNAL -period $CLK_PERIOD $CLK_INPUT
# set_dont_touch_network $CLOCK_SIGNAL
set_clock_uncertainty $CLK_UNCERT [get_clocks $CLOCK_SIGNAL]
set_input_delay $IN_DELAY -max -clock $CLOCK_SIGNAL \
[remove_from_collection [all_inputs] $CLK_INPUT]
set_output_delay $OUT_DELAY -max -clock $CLOCK_SIGNAL [all_outputs]

set OLOAD [load_of NangateOpenCellLibrary/BUF_X4/A]
set_load $OLOAD [all_outputs]

ungroup -all -flatten

# Create the netlist and the delay file
change_names -hierarchy -rules verilog
write -f verilog -hierarchy -output $TOP_ENTITY.v


compile

# Report results
report_timing > $REPORT_TIMING
report_area > $REPORT_AREA

quit
