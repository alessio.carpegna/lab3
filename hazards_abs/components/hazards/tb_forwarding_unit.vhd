library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity tb_forwarding_unit is
end entity tb_forwarding_unit;

architecture simulation of tb_forwarding_unit is

component forwarding_unit is
port(
	-- Input signals
	ex_mem_rd	: in std_logic_vector(4 downto 0);
	mem_wb_rd	: in std_logic_vector(4 downto 0);
	rs1		: in std_logic_vector(4 downto 0);
	rs2		: in std_logic_vector(4 downto 0);
	-- Output signals
	forward_a	: out std_logic_vector(1 downto 0);
	forward_b	: out std_logic_vector(1 downto 0)	
);
end component forwarding_unit;

signal test_ex_mem_rd	: std_logic_vector(4 downto 0);
signal test_mem_wb_rd	: std_logic_vector(4 downto 0);
signal test_rs1		: std_logic_vector(4 downto 0);
signal test_rs2		: std_logic_vector(4 downto 0);

signal test_forward_a	: std_logic_vector(1 downto 0);
signal test_forward_b	: std_logic_vector(1 downto 0);

begin

DUT : forwarding_unit port map(
	
	ex_mem_rd	=> test_ex_mem_rd,	
	mem_wb_rd	=> test_mem_wb_rd,
	rs1		=> test_rs1,
	rs2		=> test_rs2,
	forward_a	=> test_forward_a,
	forward_b	=> test_forward_b			
);


simulation_values : process

begin

	test_rs1 	<= "00111";
	test_rs2	<= "00111";
	test_ex_mem_rd 	<= "00111";
	test_mem_wb_rd	<= "11110";
	
	
	wait for 10 ns;

	test_rs1 	<= "00111";
	test_rs2	<= "00111";
	test_ex_mem_rd 	<= "00111";
	test_mem_wb_rd	<= "00111";
	

	wait for 10 ns;

	test_rs1 	<= "00111";
	test_rs2	<= "11000";
	test_ex_mem_rd 	<= "01010";
	test_mem_wb_rd	<= "11111";
	
	wait for 10 ns;

	test_rs1 	<= "00111";
	test_rs2	<= "00111";
	test_ex_mem_rd 	<= "01010";
	test_mem_wb_rd	<= "00111";
	
	wait for 10 ns;

	test_rs1 	<= "00000";
	test_rs2	<= "00111";
	test_ex_mem_rd 	<= "01010";
	test_mem_wb_rd	<= "00111";

	wait for 10 ns;

	test_rs1 	<= "00111";
	test_rs2	<= "00000";
	test_ex_mem_rd 	<= "01010";
	test_mem_wb_rd	<= "00111";

	wait for 10 ns;
	test_rs1 	<= "00000";
	test_rs2	<= "00000";
	test_ex_mem_rd 	<= "01010";
	test_mem_wb_rd	<= "00111";

	wait;

end process simulation_values;

end architecture simulation;
