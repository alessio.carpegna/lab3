library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity forwarding_unit is
port(
	-- Input signals
	ex_mem_rd	: in std_logic_vector(4 downto 0);
	mem_wb_rd	: in std_logic_vector(4 downto 0);
	rs1		: in std_logic_vector(4 downto 0);
	rs2		: in std_logic_vector(4 downto 0);
	-- Output signals
	forward_a	: out std_logic_vector(1 downto 0);
	forward_b	: out std_logic_vector(1 downto 0)	
);
end entity forwarding_unit;

architecture behaviour of forwarding_unit is

begin


	forward_a_gen : process (ex_mem_rd,mem_wb_rd,rs1)

	begin

	if rs1 /= "00000"  then
		if rs1 = ex_mem_rd then
			forward_a <= "10";
		elsif rs1 = mem_wb_rd then
			forward_a <= "01";
		else
			forward_a <= "00";
		end if;
	else
		forward_a <= "00";
	end if;	
	end process forward_a_gen;



	forward_b_gen : process (ex_mem_rd,mem_wb_rd,rs2)

	begin
	if rs2 /= "00000" then 
		if rs2 = ex_mem_rd then
			forward_b <= "10";
		elsif rs2 = mem_wb_rd then
			forward_b <= "01";
		else
			forward_b <= "00";
		end if;
	else
		forward_b <= "00";	
	end if;
	end process forward_b_gen;

	

end architecture behaviour;
