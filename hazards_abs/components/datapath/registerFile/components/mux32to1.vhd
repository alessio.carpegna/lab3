library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity mux32to1 is
	generic(
		-- inputs parallelism
		L_inputs	: integer := 32		
	);
	port(
		-- input signals
		sel		: in std_logic_vector(4 downto 0);
		muxIn0		: in signed(L_inputs-1 downto 0);
		muxIn1		: in signed(L_inputs-1 downto 0);
		muxIn2		: in signed(L_inputs-1 downto 0);
		muxIn3		: in signed(L_inputs-1 downto 0);
		muxIn4		: in signed(L_inputs-1 downto 0);
		muxIn5		: in signed(L_inputs-1 downto 0);
		muxIn6		: in signed(L_inputs-1 downto 0);
		muxIn7		: in signed(L_inputs-1 downto 0);
		muxIn8		: in signed(L_inputs-1 downto 0);
		muxIn9		: in signed(L_inputs-1 downto 0);
		muxIn10		: in signed(L_inputs-1 downto 0);
		muxIn11		: in signed(L_inputs-1 downto 0);
		muxIn12		: in signed(L_inputs-1 downto 0);
		muxIn13		: in signed(L_inputs-1 downto 0);
		muxIn14		: in signed(L_inputs-1 downto 0);
		muxIn15		: in signed(L_inputs-1 downto 0);
		muxIn16		: in signed(L_inputs-1 downto 0);
		muxIn17		: in signed(L_inputs-1 downto 0);
		muxIn18		: in signed(L_inputs-1 downto 0);
		muxIn19		: in signed(L_inputs-1 downto 0);
		muxIn20		: in signed(L_inputs-1 downto 0);
		muxIn21		: in signed(L_inputs-1 downto 0);
		muxIn22		: in signed(L_inputs-1 downto 0);
		muxIn23		: in signed(L_inputs-1 downto 0);
		muxIn24		: in signed(L_inputs-1 downto 0);
		muxIn25		: in signed(L_inputs-1 downto 0);
		muxIn26		: in signed(L_inputs-1 downto 0);
		muxIn27		: in signed(L_inputs-1 downto 0);
		muxIn28		: in signed(L_inputs-1 downto 0);
		muxIn29		: in signed(L_inputs-1 downto 0);
		muxIn30		: in signed(L_inputs-1 downto 0);
		muxIn31		: in signed(L_inputs-1 downto 0);

		--output signal
		muxOut		: out signed(L_inputs-1 downto 0)
	);
end entity mux32to1;

architecture behaviour of mux32to1 is
begin

	with sel select muxOut <=
		muxIn0 when "00000",	-- 0
		muxIn1 when "00001",	-- 1
		muxIn2 when "00010",	-- 2
		muxIn3 when "00011",	-- 3
		muxIn4 when "00100",	-- 4
		muxIn5 when "00101",	-- 5
		muxIn6 when "00110",	-- 6
		muxIn7 when "00111",	-- 7
		muxIn8 when "01000",	-- 8
		muxIn9 when "01001",	-- 9
		muxIn10 when "01010",	-- 10
		muxIn11 when "01011",	-- 11
		muxIn12 when "01100",	-- 12
		muxIn13 when "01101",	-- 13
		muxIn14 when "01110",	-- 14
		muxIn15 when "01111",	-- 15
		muxIn16 when "10000",	-- 16
		muxIn17 when "10001",	-- 17
		muxIn18 when "10010",	-- 18
		muxIn19 when "10011",	-- 19
		muxIn20 when "10100",	-- 20
		muxIn21 when "10101",	-- 21
		muxIn22 when "10110",	-- 22
		muxIn23 when "10111",	-- 23
		muxIn24 when "11000",	-- 24
		muxIn25 when "11001",	-- 25
		muxIn26 when "11010",	-- 26
		muxIn27 when "11011",	-- 27
		muxIn28 when "11100",	-- 28
		muxIn29 when "11101",	-- 29
		muxIn30 when "11110",	-- 30
		muxIn31 when others;	-- 31

	
end architecture behaviour;
