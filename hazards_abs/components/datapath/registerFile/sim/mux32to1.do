set DUT "../components/mux32to1.vhd"
set TB "mux32to1_tb"
set TB_DIR "../tb"

vcom $DUT
vcom $TB_DIR/$TB.vhd

vsim -t ns -novopt work.$TB
run 1us

quit -f
