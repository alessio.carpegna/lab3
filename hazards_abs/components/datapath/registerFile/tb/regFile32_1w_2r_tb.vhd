library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity regFile32_1w_2r_tb is
end entity regFile32_1w_2r_tb;

architecture test of regFile32_1w_2r_tb is

	component regFile32_1w_2r is
		generic(
			regParallelism	: integer := 32		
		);
		port(
			-- input signals
			writeAddress	: in std_logic_vector(4 downto 0);
			readAddress1	: in std_logic_vector(4 downto 0);
			readAddress2	: in std_logic_vector(4 downto 0);
			dataIn		: in signed(regParallelism-1 downto 0);
			writeEn		: in std_logic;
			clk		: in std_logic;

			-- output signals
			readReg1	: out signed(regParallelism-1 downto 0);
			readReg2	: out signed(regParallelism-1 downto 0)

		);
	end component regFile32_1w_2r;

	-- input signals
	signal writeAddress	: std_logic_vector(4 downto 0);
	signal readAddress1	: std_logic_vector(4 downto 0);
	signal readAddress2	: std_logic_vector(4 downto 0);
	signal dataIn		: signed(31 downto 0);
	signal writeEn		: std_logic;
	signal clk		: std_logic;

	-- output signals
	signal readReg1	: signed(31 downto 0);
	signal readReg2	: signed(31 downto 0);



begin

	clkGen : process
	begin
		clk <= '0';
		wait for 10 ns;
		clk <= '1';
		wait for 10 ns;
	end process clkGen;


	testing : process
	begin

		wait for 20 ns;
	
		-- write in all the registers	
		writeLoop : for i in 0 to 31 loop 
			writeAddress	<= std_logic_vector(to_unsigned(i,5));
			writeEn		<= '1';
			dataIn		<= to_signed(i+1, 32);
			readAddress1	<= std_logic_vector(to_unsigned(0,5));
			readAddress2	<= std_logic_vector(to_unsigned(3,5));
			wait for 20 ns;
		end loop writeLoop;

		-- read from all the registers
		readLoop : for i in 0 to 15 loop
			readAddress1	<= std_logic_vector(to_unsigned(2*i,5));
			readAddress2	<= std_logic_vector(to_unsigned(2*i+1,5));
			wait for 20 ns;
		end loop readLoop;

		-- try to write with writeEn to '0'
		dataIn 		<= to_signed(127, 32);
		writeAddress	<= std_logic_vector(to_unsigned(3,5));
		readAddress1	<= std_logic_vector(to_unsigned(3,5));
		readAddress2	<= std_logic_vector(to_unsigned(3,5));

	end process testing;

	

	DUT : regFile32_1w_2r
		generic map(
			regParallelism	=> 32		
		)
		port map(
			-- input signals
			writeAddress	=> writeAddress,
			readAddress1	=> readAddress1,
			readAddress2	=> readAddress2,
			dataIn		=> dataIn,
			writeEn		=> writeEn,
			clk		=> clk,

			-- output signals
			readReg1	=> readReg1,
			readReg2	=> readReg2
		);

end architecture test;
