library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity tb_alu is
end tb_alu;

architecture tb of tb_alu is

component alu is
  port( 
  
		alu_operation	: in signed(3 downto 0);
		rs1				: in signed(31 downto 0);
		rs2				: in signed(31 downto 0);
		
		rd		: out signed(31 downto 0);
		zero	: out std_logic
   );
end component alu;

signal tb_alu_operation: signed (3 downto 0);
signal tb_rs1, tb_rs2, tb_rd : signed (31 downto 0);
signal tb_zero: std_logic;

BEGIN

DUT: alu port map(

	alu_operation	=> tb_alu_operation,
	
	rs1		=> tb_rs1,
	rs2		=> tb_rs2,
	
	rd		=> tb_rd,
	zero	=> tb_zero	
);

operations : process

	begin
		
		tb_rs1	<= "11010100111101011010101101011101";
		tb_rs2	<= "11000101010010010101111001011101";
		
		tb_alu_operation <= "0000"; -- ANDI
		wait for 15 ns;
		
		tb_alu_operation <= "0001"; -- XOR
		wait for 15 ns;
		
		tb_alu_operation <= "0011"; -- SRAI
		wait for 15 ns;
		
		tb_alu_operation <= "0101"; -- LUI
		wait for 15 ns;
		
		tb_alu_operation <= "1010"; -- AUIPC
		wait for 15 ns;
		
		tb_alu_operation <= "0100"; -- SLT
		wait for 15 ns;
		
		tb_alu_operation <= "0010"; -- SOMMA NORMALE
		wait for 15 ns;

		tb_alu_operation <= "0110"; -- JAL
		wait for 15 ns;
		
		wait;
	end process operations;

end architecture tb;