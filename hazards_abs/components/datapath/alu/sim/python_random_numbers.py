#!/usr/bin/python3

import random

#GENERATORE DI VARI PATTERN CASUALI
#Siccome acpita spesso di dover generare numeri casuali formattati in maniere specifiche in questo file raggruppiamo varie modalità di genrazione le scriviamo in delle funzioni
#in modo da poterle riutilizzare qando serve. Siccome i pattern che possono servire sono svariati man mano aggiungiamo nuove funzioni



#Funzione che genera un numero n_colonne di valori su n_bit su una riga sola e li scrive su file. Di queste righe ne scrive n_righe
#n_bit è il numero di bit su cui generare i valori
#n_righe specifica quante righe di valori stampare
#n_colobbe specifica quanti valori ci sono su ogni riga
#nome_file specifica il percorso del file su cui salvare i numeri generati
#mode seleziona la modallità di rappresentazione dei numeri: b sta per binario,dec sta per decimale

def numb_gen_matrix(n_bit,n_righe,n_colonne,nome_file,mode):
	string_to_print_bin= ""
	string_dec=""
	f=open(nome_file,'w')
	f2=open("dec_"+nome_file,'w')
	for i in range(0,n_righe):			#for che dice quante righe stampare
		for k in range(0,n_colonne):
			n_printed = random.randint(0,(2**n_bit)-1)
			if k < n_colonne-1:
				string_to_print_bin=string_to_print_bin + str(format(n_printed,'0'+str(n_bit)+'b'))+' '
				string_dec = string_dec+str(n_printed)+' '
			else:
				string_to_print_bin=string_to_print_bin + str(format(n_printed,'0'+str(n_bit)+'b'))+'\n'
				string_dec = string_dec+str(n_printed)+'\n'
		if mode=="b":
			f.write(string_to_print_bin)
		if mode=="dec":
			f.write(string_dec)
		if mode=="bd":
		 	f.write(string_to_print_bin)
		 	f2.write(string_dec)
		string_to_print_bin=""
		string_dec=""
	f.close()
	f2.close()

mode='bd'
nome_file="comp_equal_inputs.txt"
numb_gen_matrix(32,5,2,nome_file,mode)
