library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity srai is
	generic(
		shamtLength	: integer := 5
			
	);
	port(
		-- Value to shift
		rs1	: in signed(2**shamtLength-1 downto 0);
		-- Shift amount (shamt)
		imm	: in signed(shamtLength-1 downto 0);
		
		-- Output signals
		sraiOut	: out signed(2**shamtLength-1 downto 0)	
	);
end entity srai;

architecture behaviour of srai is


	component mux32to1 is
		generic(
			-- inputs parallelism
			L_inputs	: integer := 32		
		);
		port(
			-- input signals
			sel		: in std_logic_vector(4 downto 0);
			muxIn0		: in signed(L_inputs-1 downto 0);
			muxIn1		: in signed(L_inputs-1 downto 0);
			muxIn2		: in signed(L_inputs-1 downto 0);
			muxIn3		: in signed(L_inputs-1 downto 0);
			muxIn4		: in signed(L_inputs-1 downto 0);
			muxIn5		: in signed(L_inputs-1 downto 0);
			muxIn6		: in signed(L_inputs-1 downto 0);
			muxIn7		: in signed(L_inputs-1 downto 0);
			muxIn8		: in signed(L_inputs-1 downto 0);
			muxIn9		: in signed(L_inputs-1 downto 0);
			muxIn10		: in signed(L_inputs-1 downto 0);
			muxIn11		: in signed(L_inputs-1 downto 0);
			muxIn12		: in signed(L_inputs-1 downto 0);
			muxIn13		: in signed(L_inputs-1 downto 0);
			muxIn14		: in signed(L_inputs-1 downto 0);
			muxIn15		: in signed(L_inputs-1 downto 0);
			muxIn16		: in signed(L_inputs-1 downto 0);
			muxIn17		: in signed(L_inputs-1 downto 0);
			muxIn18		: in signed(L_inputs-1 downto 0);
			muxIn19		: in signed(L_inputs-1 downto 0);
			muxIn20		: in signed(L_inputs-1 downto 0);
			muxIn21		: in signed(L_inputs-1 downto 0);
			muxIn22		: in signed(L_inputs-1 downto 0);
			muxIn23		: in signed(L_inputs-1 downto 0);
			muxIn24		: in signed(L_inputs-1 downto 0);
			muxIn25		: in signed(L_inputs-1 downto 0);
			muxIn26		: in signed(L_inputs-1 downto 0);
			muxIn27		: in signed(L_inputs-1 downto 0);
			muxIn28		: in signed(L_inputs-1 downto 0);
			muxIn29		: in signed(L_inputs-1 downto 0);
			muxIn30		: in signed(L_inputs-1 downto 0);
			muxIn31		: in signed(L_inputs-1 downto 0);

			--output signal
			muxOut		: out signed(L_inputs-1 downto 0)
		);
	end component mux32to1;

	type matrix32x32 is array(31 downto 0) of signed(31 downto 0);
	signal shifted_rs1	: matrix32x32;

begin


	shifted_rs1(0)			<= rs1;
	shifted_rs1(1)(31 downto 30)	<= (others => rs1(31));
	shifted_rs1(1)(29 downto 0)	<= rs1(30 downto 1);
	shifted_rs1(2)(31 downto 29)	<= (others => rs1(31));
	shifted_rs1(2)(28 downto 0)	<= rs1(30 downto 2);
	shifted_rs1(3)(31 downto 28)	<= (others => rs1(31));
	shifted_rs1(3)(27 downto 0)	<= rs1(30 downto 3);
	shifted_rs1(4)(31 downto 27)	<= (others => rs1(31));
	shifted_rs1(4)(26 downto 0)	<= rs1(30 downto 4);
	shifted_rs1(5)(31 downto 26)	<= (others => rs1(31));
	shifted_rs1(5)(25 downto 0)	<= rs1(30 downto 5);
	shifted_rs1(6)(31 downto 25)	<= (others => rs1(31));
	shifted_rs1(6)(24 downto 0)	<= rs1(30 downto 6);
	shifted_rs1(7)(31 downto 24)	<= (others => rs1(31));
	shifted_rs1(7)(23 downto 0)	<= rs1(30 downto 7);
	shifted_rs1(8)(31 downto 23)	<= (others => rs1(31));
	shifted_rs1(8)(22 downto 0)	<= rs1(30 downto 8);
	shifted_rs1(9)(31 downto 22)	<= (others => rs1(31));
	shifted_rs1(9)(21 downto 0)	<= rs1(30 downto 9);
	shifted_rs1(10)(31 downto 21)	<= (others => rs1(31));
	shifted_rs1(10)(20 downto 0)	<= rs1(30 downto 10);
	shifted_rs1(11)(31 downto 20)	<= (others => rs1(31));
	shifted_rs1(11)(19 downto 0)	<= rs1(30 downto 11);
	shifted_rs1(12)(31 downto 19)	<= (others => rs1(31));
	shifted_rs1(12)(18 downto 0)	<= rs1(30 downto 12);
	shifted_rs1(13)(31 downto 18)	<= (others => rs1(31));
	shifted_rs1(13)(17 downto 0)	<= rs1(30 downto 13);
	shifted_rs1(14)(31 downto 17)	<= (others => rs1(31));
	shifted_rs1(14)(16 downto 0)	<= rs1(30 downto 14);
	shifted_rs1(15)(31 downto 16)	<= (others => rs1(31));
	shifted_rs1(15)(15 downto 0)	<= rs1(30 downto 15);
	shifted_rs1(16)(31 downto 15)	<= (others => rs1(31));
	shifted_rs1(16)(14 downto 0)	<= rs1(30 downto 16);
	shifted_rs1(17)(31 downto 14)	<= (others => rs1(31));
	shifted_rs1(17)(13 downto 0)	<= rs1(30 downto 17);
	shifted_rs1(18)(31 downto 13)	<= (others => rs1(31));
	shifted_rs1(18)(12 downto 0)	<= rs1(30 downto 18);
	shifted_rs1(19)(31 downto 12)	<= (others => rs1(31));
	shifted_rs1(19)(11 downto 0)	<= rs1(30 downto 19);
	shifted_rs1(20)(31 downto 11)	<= (others => rs1(31));
	shifted_rs1(20)(10 downto 0)	<= rs1(30 downto 20);
	shifted_rs1(21)(31 downto 10)	<= (others => rs1(31));
	shifted_rs1(21)(9 downto 0)	<= rs1(30 downto 21);
	shifted_rs1(22)(31 downto 9)	<= (others => rs1(31));
	shifted_rs1(22)(8 downto 0)	<= rs1(30 downto 22);
	shifted_rs1(23)(31 downto 8)	<= (others => rs1(31));
	shifted_rs1(23)(7 downto 0)	<= rs1(30 downto 23);
	shifted_rs1(24)(31 downto 7)	<= (others => rs1(31));
	shifted_rs1(24)(6 downto 0)	<= rs1(30 downto 24);
	shifted_rs1(25)(31 downto 6)	<= (others => rs1(31));
	shifted_rs1(25)(5 downto 0)	<= rs1(30 downto 25);
	shifted_rs1(26)(31 downto 5)	<= (others => rs1(31));
	shifted_rs1(26)(4 downto 0)	<= rs1(30 downto 26);
	shifted_rs1(27)(31 downto 4)	<= (others => rs1(31));
	shifted_rs1(27)(3 downto 0)	<= rs1(30 downto 27);
	shifted_rs1(28)(31 downto 3)	<= (others => rs1(31));
	shifted_rs1(28)(2 downto 0)	<= rs1(30 downto 28);
	shifted_rs1(29)(31 downto 2)	<= (others => rs1(31));
	shifted_rs1(29)(1 downto 0)	<= rs1(30 downto 29);
	shifted_rs1(30)(31 downto 1)	<= (others => rs1(31));
	shifted_rs1(30)(0)		<= rs1(30);
	shifted_rs1(31)(31 downto 0)	<= (others => rs1(31));

	MUX : mux32to1 
		generic map(
			-- inputs parallelism
			L_inputs 	=>32		
		)
		port map(
			-- input signals
			sel		=> std_logic_vector(imm), 	
			muxIn0		=> shifted_rs1(0), 	
			muxIn1		=> shifted_rs1(1), 	
			muxIn2		=> shifted_rs1(2), 	
			muxIn3		=> shifted_rs1(3), 	
			muxIn4		=> shifted_rs1(4), 	
			muxIn5		=> shifted_rs1(5), 	
			muxIn6		=> shifted_rs1(6), 	
			muxIn7		=> shifted_rs1(7), 	
			muxIn8		=> shifted_rs1(8), 	
			muxIn9		=> shifted_rs1(9), 	
			muxIn10		=> shifted_rs1(10), 	
			muxIn11		=> shifted_rs1(11), 	
			muxIn12		=> shifted_rs1(12), 	
			muxIn13		=> shifted_rs1(13), 	
			muxIn14		=> shifted_rs1(14), 	
			muxIn15		=> shifted_rs1(15), 	
			muxIn16		=> shifted_rs1(16), 	
			muxIn17		=> shifted_rs1(17), 	
			muxIn18		=> shifted_rs1(18), 	
			muxIn19		=> shifted_rs1(19), 	
			muxIn20		=> shifted_rs1(20), 	
			muxIn21		=> shifted_rs1(21), 	
			muxIn22		=> shifted_rs1(22), 	
			muxIn23		=> shifted_rs1(23), 	
			muxIn24		=> shifted_rs1(24), 	
			muxIn25		=> shifted_rs1(25), 	
			muxIn26		=> shifted_rs1(26), 	
			muxIn27		=> shifted_rs1(27), 	
			muxIn28		=> shifted_rs1(28), 	
			muxIn29		=> shifted_rs1(29), 	
			muxIn30		=> shifted_rs1(30), 	
			muxIn31		=> shifted_rs1(31), 	

			--output signal
			muxOut		=> sraiOut	
		);




end architecture behaviour;
