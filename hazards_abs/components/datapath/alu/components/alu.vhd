library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity alu is 
	port(
		-- input signals
		alu_operation	: in signed(3 downto 0);
		rs1		: in signed(31 downto 0);
		rs2		: in signed(31 downto 0);
		
		-- output signals
		rd		: out signed(31 downto 0);
		zero		: out std_logic
	);
end entity alu;

architecture structure of alu is 

	component abs_comp 

	port(
		value_in	: in signed(31 downto 0);
		
		abs_value	: out signed(31 downto 0)
	);
	end component abs_comp;

	component andi
		port(
		
			rs1		: in signed(31 downto 0);
			imm		: in signed(31 downto 0);
			
			andi_out	: out signed(31 downto 0)	
		);
	end component andi;
	
	component lui
		port(
		
			imm		: in signed(31 downto 0);
			
			lui_out		: out signed(31 downto 0)	
		);
	end component lui;
	
	component mux_2to1
		port(
		
			sel	: in std_logic;
			in0	: in signed(31 downto 0);
			in1	: in signed(31 downto 0);
			
			mux_out	: out signed(31 downto 0)	
		);
	end component mux_2to1;
	
	component mux_operation
		port(
		
			sel	: in signed(3 downto 0);
			in0	: in signed(31 downto 0);
			in1	: in signed(31 downto 0);
			in2	: in signed(31 downto 0);
			in3	: in signed(31 downto 0);
			in4	: in signed(31 downto 0);
			in5	: in signed(31 downto 0);
			in6	: in signed(31 downto 0);
			in7	: in signed(31 downto 0);
			in8	: in signed(31 downto 0);
			in9	: in signed(31 downto 0);
			in10	: in signed(31 downto 0);
			in11	: in signed(31 downto 0);
			in12	: in signed(31 downto 0);
			in13	: in signed(31 downto 0);
			in14	: in signed(31 downto 0);
			in15	: in signed(31 downto 0);
			
			mux_out	: out signed(31 downto 0)
		);
	end component mux_operation;
	
	component xor_block
		port(

			rs1	: in signed(31 downto 0);
			rs2	: in signed(31 downto 0);
			
			xor_out	: out signed(31 downto 0)
		);
	end component xor_block;
	
	component srai
		generic(
			shamtLength	: integer := 5
		);
		port(
			-- Value to shift
			rs1		: in signed(2**shamtLength-1 downto 0);
			-- Shift amount (shamt)
			imm		: in signed(shamtLength-1 downto 0);
			
			-- Output signals
			sraiOut		: out signed(2**shamtLength-1 downto 0)	
		);
	end component srai;
	
	component adder_32bit
		port(
			add1	: in signed(31 downto 0);
			add2	: in signed(31 downto 0);
			
			sum_out	: out signed(31 downto 0)
		);
	end component adder_32bit;
	
	component comp_equal
		port(
			-- Inputs from register file
			rs1	: in signed(31 downto 0);
			rs2	: in signed(31 downto 0);
			
			-- Output
			zero	: out std_logic
		);
	end component comp_equal;
	
	component comp_lower 
		port(
			-- Ingressi
			rs1,rs2		: in signed(31 downto 0);
			-- Uscita
			comp_lower_out	: out signed(31 downto 0)
		);
	end component comp_lower;
	
	-- input signals
	signal rs1_int			: signed(31 downto 0);
	signal rs2_int			: signed(31 downto 0);

	-- internal signals
	signal andi_out			: signed(31 downto 0);
	signal lui_out			: signed(31 downto 0);
	signal mux_adder_stage1		: signed(31 downto 0);
	signal mux_adder_stage2		: signed(31 downto 0);
	signal mux_operation_out	: signed(31 downto 0);
	signal xor_out			: signed(31 downto 0);
	signal comp_lower_out		: signed(31 downto 0);
	signal srai_out			: signed(31 downto 0);
	signal sum_out 			: signed(31 downto 0); 
	signal abs_value			: signed(31 downto 0); 

begin
	
	rs1_int <= rs1;
	rs2_int	<= rs2;
	
	ABS_BLOCK: abs_comp port map(
		
		value_in	=> rs1_int,
		
		abs_value	=> abs_value
	);
	
	XOR_OP: xor_block port map(
	
		rs1	=> rs1_int,
		rs2	=> rs2_int,
		
		xor_out	=> xor_out
	);
	
	ANDI_OP: andi port map(
	
		rs1	=> rs1_int,
		imm	=> rs2_int,
		
		andi_out	=> andi_out
	);
	
	LUI_OP: lui port map(
	
		imm	=> rs2_int,
		
		lui_out	=> lui_out
	);
	
	MUX_OP: mux_operation port map(
	
		sel	=> alu_operation,
		in0	=> andi_out,
		in1	=> xor_out,
		in2	=> sum_out,
		in3	=> srai_out,
		in4	=> comp_lower_out,
		in5	=> lui_out,
		in6	=> sum_out,
		in7	=> abs_value,
		in8	=> (others => '0'),
		in9	=> (others => '0'),
		in10	=> sum_out,
		in11	=> (others => '0'), 
		in12	=> (others => '0'),
		in13	=> (others => '0'),
		in14	=> (others => '0'),
		in15	=> (others => '0'),
		
		mux_out	=> rd
	);
	
	MUX_ADD1: mux_2to1 port map(
	
		sel	=> alu_operation(3),
		in0	=> rs2_int,
		in1	=> lui_out,
		
		mux_out	=> mux_adder_stage1
	);

	MUX_ADD2: mux_2to1 port map(
	
		sel	=> alu_operation(2),
		in0	=> mux_adder_stage1,
		in1	=> "00000000000000000000000000000100", --to_signed(4, 32),
		
		mux_out	=> mux_adder_stage2
	);

	
	SRAI_OP: srai 
		generic map(
			shamtLength	=> 5		
		)
		port map(
	
		rs1	=> rs1_int,
		imm	=> rs2_int(4 downto 0),
		
		sraiOut	=> srai_out
	);
	
	ADDER_ALU_INSTANTIATION: adder_32bit port map(
	
		add1	=> rs1_int,
		add2	=> mux_adder_stage2,
		
		sum_out	=> sum_out
	);
	
	COMP_EQUAL_OP: comp_equal port map(
	
		rs1	=> rs1_int,
		rs2	=> rs2_int,
		
		zero	=> zero
	);
	
	COMP_LOWER_OP: comp_lower port map(
	
		rs1	=> rs1_int,
		rs2	=> rs2_int,
		
		comp_lower_out	=> comp_lower_out
	);

end architecture structure;
