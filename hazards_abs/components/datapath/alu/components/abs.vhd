library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity abs_comp is 

	port(
		value_in	: in signed(31 downto 0);
		
		abs_value	: out signed(31 downto 0)
	);
end entity abs_comp;

architecture structure of abs_comp is 

signal msb_to_xor	: signed(31 downto 0);
signal ca1_value	: signed(31 downto 0);
signal add_value	: signed(31 downto 0);

begin

	-- Generation of bit used to xor, depending on the pos/neg value
		msb_to_xor <= (others => value_in(31));

	-- XOR of the value: inversion if it is negative
		ca1_value	<= value_in XOR msb_to_xor;
		
	-- Generation of the value to be added to perform CA2 in negative case
		add_value(31 downto 1)	<= (others => '0');
		add_value(0)		<= value_in(31);

	-- Sum to obtain final value: 
		abs_value	<= add_value + ca1_value;
	

end architecture structure;