#!/usr/bin/bash


TCL_SCRIPT="$1"
DESIGN_FILE="$2"
# Set up the environment to properly run Modelsim
source /software/scripts/init_innovus17.11

# Execute the desired set of Modelsim commands
innovus -no_gui -batch -files $TCL_SCRIPT

