vlog ../syn/uP.v
vcom ../tb/tb_uP_mem_version2.vhd
vcom ../components/externalMemories/components/memory.vhd
vcom ../components/muxes/mux_2to1_1bit.vhd
vcom ../components/muxes/mux_2to1_std_logic.vhd
vlog NangateOpenCellLibrary.v

vsim -t ns -novopt work.tb_uP_mem_version2
run 15 us

quit -f
