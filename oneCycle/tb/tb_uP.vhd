library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity tb_uP is
end tb_uP;

architecture tb of tb_uP is

component uP is
  port( 
  
		clk			: in std_logic;
		rst			: in std_logic;
		
		IM_instruction	: in std_logic_vector(31 downto 0);
		read_data		: in std_logic_vector(31 downto 0);

		ALU_result	: out std_logic_vector(31 downto 0);
		writeData	: out std_logic_vector(31 downto 0);
		PC			: out std_logic_vector(31 downto 0);	
		memWrite	: out std_logic;
		memRead		: out std_logic;
		
		--- per tb ---
		tb_CU_istructions: out std_logic_vector(11 downto 0)
		--------------
   );
end component uP;

-- vectors of instructions
type matrix is array(0 to 16) of std_logic_vector(31 downto 0);

signal tb_clk, tb_rst: std_logic;
signal tb_IM_instruction, tb_read_data,  tb_ALU_result, tb_writeData, tb_PC: std_logic_vector (31 downto 0);
signal tb_CU_istructions: std_logic_vector(11 downto 0);
signal tb_memWrite, tb_memRead: std_logic;
signal instructions_vect: matrix;
signal i    : unsigned(4 downto 0) := "00000";

BEGIN

	tb_read_data <= "00000000000000000000000000000001";

	DUT: uP port map(

		clk		=> tb_clk,
		rst		=> tb_rst,
		IM_instruction	=> tb_IM_instruction,
		read_data	=> tb_read_data,
		ALU_result	=> tb_ALU_result,
		writeData	=> tb_writeData,
		PC			=> tb_PC,
		memWrite	=> tb_memWrite,
		memRead		=> tb_memRead,
		
		tb_CU_istructions => tb_CU_istructions
	);

	clock_gen: process
	begin
		if i >= 17 then
		tb_clk <= '0';
			wait for 10 ns;
		tb_clk <= '1';
			wait for 10 ns;
		end if;	
	end process clock_gen;
	
	instruction_gen : process(tb_clk)

	begin

		tb_read_data <= "00000000000000000000000000000001";
		
		instructions_vect <= (
		"00000000000000000000001000110111", -- LUI 0, X4
		"00000010100000100000001000010011", -- ADDI 40, X4, X4
		"00000000110000100010001010000011", -- LW X5, 12(X4)
		"00000001000000100010001100000011", -- LW X6, 16(X4)
		"00000000010100110000001100010011", -- ADDI 5, X6, X6
		"00000000010100110000001110110011", -- ADD X7, X5, X6
		"00000010011100100010111000100011", -- SW X7, 60(X4)
		"00000000000100110111010000010011", -- ANDI 1, X6, X8 
		"00000100100000100010000000100011", -- SW X8, 64(X4)
		"00000000010100110100010010110011", -- XOR X9, X5, X6		   
		"00000100100100100010001000100011", -- SW X9, 68(X4)
		"00000000010100110010010100110011", -- SLT X5, X6, X10		
 		"00000000110000000000011111101111", -- JAL 12, X15
		"00000000110000100010010110000011", -- LW X11, 12(X4)
		"00000000010101011000101001100011", -- BEQ 20, X5, X11
		"01000000000100101101011000100000", -- SRAI 1, X5, X12
		"00000000000000000100011010010111"  -- AUIPC 4, X13
		);
		
		if tb_clk'event and tb_clk = '1' then 
			if (i < 17) then
				tb_IM_instruction <= instructions_vect( to_integer(i) );
				i <= i+ 1;
			end if;
		end if;
	end process instruction_gen;

	rst_gen: process
	begin	
		tb_rst <= '0';
		wait for 5 ns;
		tb_rst <= '1';
		wait;
	end process rst_gen;
	
end architecture tb;