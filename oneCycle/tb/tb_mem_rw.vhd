library ieee;
use ieee.std_logic_1164.all;
use std.textio.all;
use ieee.std_logic_textio.all;
use ieee.numeric_std.all;

entity tb_mem_rw is
end entity tb_mem_rw;

architecture simulation of tb_mem_rw is

component memory is
	generic(
		startingAddress	: integer := 0;
		endAddress	: integer := 8;
		parallelism	: integer := 32		
	);
	port(
		-- input signals
		address		: in std_logic_vector(parallelism downto 0);
		writeData	: in std_logic_vector(parallelism-1 downto 0);
		memRead		: in std_logic;
		memWrite	: in std_logic;
		-- output signals
		readData	: out std_logic_vector(parallelism-1 downto 0)
	);
end component memory;

-- Data memory signals 
signal dataAddress	: std_logic_vector(31 downto 0);
signal writeData	: std_logic_vector(31 downto 0);
signal memRead		: std_logic;
signal memWrite		: std_logic;
signal readData		: std_logic_vector(31 downto 0);

-- Instruction memory signals

signal instrAddress	: std_logic_vector(31 downto 0);
signal extData		: std_logic_vector(31 downto 0);
signal readInstr	: std_logic;
signal loadInstr	: std_logic;
signal IMinstruction	: std_logic_vector(31 downto 0);

file fileData	: text open read_mode is "";
file fileInstr	: text open read_mode is "";


begin

DM_UT : memory generic map(
			startingAddress => 0,
			endAddress	=> 21,
			parallelism	=> 32
		) 
		port map(
			address 	=> dataAddress,		
			writeData	=> writeData,
			memRead		=> memRead,
			memWrite	=> memWrite,
			readData	=> readData
		);

IM_UT : memory generic map(
			startingAddress => 0,
			endAddress	=> 21,
			parallelism	=> 32
		) 
		port map(
			address		=> instrAddress,		
			writeData	=> extData,
			memRead		=> readInstr,
			memWrite	=> loadInstr,
			readData	=> IMinstruction
		);

DM_loading_and_reading : process

variable inLine_data		: line;
variable dataFromFile		: std_logic_vector(31 downto 0);
variable i			: integer;
begin
	memRead 	<= '0';
	memWrite	<= '1';

	i := 264306688;
	while not endfile(fileData) loop
				
		dataAddress	<= std_logic_vector(to_unsigned(i, 32));

		readline(fileData, inLine_data);
		read(inLine_data, dataFromFile);
		writeData	<= dataFromFile;

		i := i+1;
		wait for 1 ns;
	end loop;

	wait for 30 ns;
	memRead		<= '1';
	memWrite	<= '0';
	for i in 264306688 to 264306695 loop
		instrAddress <= std_logic_vector(to_unsigned(i, 32));
		wait for 1 ns;
	end loop;



end process DM_loading_and_reading;

IM_loading_and_reading : process

variable inLine_instr		: line;
variable instrFromFile		: std_logic_vector(31 downto 0);
variable i 			: integer;

begin
readInstr <= '0';
loadInstr <= '1';

	i := 0;
	while not endfile(fileInstr) loop
		instrAddress	<= std_logic_vector(to_unsigned(i,32));

		readline(fileInstr,inLine_instr);
		read(inLine_instr,instrFromFile);
		extData		<= instrFromFile;

		i := i+1;
		wait for 1 ns;
	end loop;
	wait for 30 ns;
	readInstr <= '1';
	loadInstr <= '0';
	for i in 0 to 21 loop
		dataAddress <= std_logic_vector(to_unsigned(i,32));
	end loop;



end process IM_loading_and_reading;
end architecture simulation;






