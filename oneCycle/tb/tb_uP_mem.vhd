library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use std.textio.all;
use ieee.std_logic_textio.all;

entity tb_uP_mem is
end entity tb_uP_mem;

architecture test of tb_uP_mem is
	
	component memory is
		generic(
			startingAddress	: integer := 0;
			endAddress	: integer := 8;
			parallelism	: integer := 32		
		);
		port(
			-- input signals
			address		: in std_logic_vector(parallelism downto 0);
			writeData	: in std_logic_vector(parallelism-1 downto 0);
			memRead		: in std_logic;
			memWrite	: in std_logic;

			-- output signals
			readData	: out std_logic_vector(parallelism-1 downto 0)
		);
	end component memory;

	component uP is
		port(
			clk		: in std_logic;
			rst		: in std_logic;
			
			IM_instruction	: in std_logic_vector(31 downto 0);
			read_data		: in std_logic_vector(31 downto 0);

			ALU_result	: out std_logic_vector(31 downto 0);
			writeData	: out std_logic_vector(31 downto 0);
			PC			: out std_logic_vector(31 downto 0);	
			memWrite	: out std_logic;
			memRead		: out std_logic;
			
			--- per tb ---
			tb_CU_istructions: out std_logic_vector(11 downto 0)
			--------------
		);
	end component uP;

	component mux_2to1_std_logic is 

		port(
			sel	: in std_logic;
			in0	: in std_logic_vector(31 downto 0);
			in1	: in std_logic_vector(31 downto 0);
			
			mux_out	: out std_logic_vector(31 downto 0)
		);
	end component mux_2to1_std_logic;
	
	component mux_2to1_1bit is
	
		port(
		sel		: in std_logic;
		in0		: in std_logic;
		in1		: in std_logic;
		
		mux_out	: out std_logic
	);
	end component mux_2to1_1bit;

	file fileData	: text open read_mode is "C:\Modelsim_files\dataSection.txt";
	file fileIstructions			: text open read_mode is "C:\Modelsim_files\textSection.txt";

	signal clk				: std_logic;
	signal rst				: std_logic;
	signal IM_instruction	: std_logic_vector(31 downto 0);
	signal read_data		: std_logic_vector(31 downto 0);
	signal ALU_result		: std_logic_vector(31 downto 0);
	signal dataAddress		: std_logic_vector(31 downto 0);
	signal writeData_uP		: std_logic_vector(31 downto 0);
	signal PC				: std_logic_vector(31 downto 0);
	signal memWrite_uP		: std_logic;
	signal memRead_uP		: std_logic;

	-- signals used to load data in the data memory
	signal loadExt		: std_logic;
	signal extData		: std_logic_vector(31 downto 0);
	signal extAddress_data	: std_logic_vector(31 downto 0);
	signal extAddress_instr	: std_logic_vector(31 downto 0);

	-- signals used by the testbench to load the code used to
	-- test the uP into the instruction memory
	signal loadInstr		: std_logic;
	signal readInstr		: std_logic;
	signal ext_memRead		: std_logic; 
	signal ext_memWrite		: std_logic;
	signal writeData		: std_logic_vector(31 downto 0);
	signal memRead			: std_logic;
	signal memWrite			: std_logic;
	signal instAddress		: std_logic_vector(31 downto 0);

	signal clk_activate		: std_logic;

begin
	
	clk_activate <= clk AND not(loadExt);

	processor : uP	-- MICROPROCESSOR
		port map(
			-- input signals
			clk			=> clk_activate,
			rst			=> rst,
			IM_instruction	=> IM_instruction,
			read_data	=> read_data,

			-- output signals
			ALU_result	=> ALU_result,
			writeData	=> writeData_uP,
			PC			=> PC,
			memWrite	=> memWrite_uP,
			memRead		=> memRead_uP
		);

	dataMemory : memory  -- DATA MEMORY
		generic map(
			startingAddress	=> 66076671,
			endAddress	=> 66076681,
			parallelism		=> 32		
		)
		port map(
			-- input signals
			address		=> dataAddress(31 downto 2),
			writeData	=> writeData,
			memRead		=> memRead,
			memWrite	=> memWrite,

			-- output signals
			readData	=> read_data
		);

	instructionMemory : memory  -- INSTRUCTION MEMORY
		generic map(
			startingAddress	=> 0,
			endAddress	=> 100,
			parallelism		=> 32		
		)
		port map(
			-- input signals
			address		=> instAddress(31 downto 2),
			writeData	=> extData,
			memRead		=> readInstr,
			memWrite	=> loadInstr,

			-- output signals
			readData	=> IM_instruction
		);
		
	address_instrMEM_mux : mux_2to1_std_logic -- SELECTS ADDRESS FOR THE INSTRUCTION MEMORY
		port map(
			-- input signals
			sel		=> loadExt,
			in0		=> PC,
			in1		=> extAddress_instr,
			
			-- output signals
			mux_out		=> instAddress
		);
		
	address_dataMEM_mux : mux_2to1_std_logic -- SELECTS ADDRESS FOR THE DATA MEMORY
		port map(
			-- input signals
			sel		=> loadExt,
			in0		=> ALU_result,
			in1		=> extAddress_data,
			
			-- output signals
			mux_out		=> dataAddress
		);
		
	data_dataMEM_mux : mux_2to1_std_logic -- SELECTS DATA FOR THE DATA MEMORY
		port map(
			-- input signals
			sel		=> loadExt,
			in0		=> writeData_uP,
			in1		=> extData,
			
			-- output signals
			mux_out		=> writeData
		);
		
	memWrite_dataMEM : mux_2to1_1bit -- SELECT memWrite OPERATION FOR THE DATA MEMORY FROM uP OR EXT
		port map(
			-- input signals
			sel		=> loadExt,
			in0		=> memWrite_uP,
			in1		=> ext_memWrite,
			
			-- output signals
			mux_out		=> memWrite
		);
		
	memRead_dataMEM : mux_2to1_1bit -- SELECT WRITE OPERATION FOR THE DATA MEMORY FROM uP OR EXT
		port map(
			-- input signals
			sel		=> loadExt,
			in0		=> memRead_uP,
			in1		=> ext_memRead,
			
			-- output signals
			mux_out		=> memRead
		);
		
	clk_gen: process
	begin
		clk <= '0';
		wait for 5 ns;
		clk <= '1';
		wait for 5 ns;
	end process clk_gen;
		
	data_gen: process
	
		variable inLine_data		: line;
		variable inLine_instruction	: line;

		variable instruction		: std_logic_vector(31 downto 0);
		variable data				: std_logic_vector(31 downto 0);

		variable i			: integer := 0;
		
		begin
		
		-- UPLOAD THE MEMORIES
			rst		<= '0';
			loadExt <= '1';
			
		-- Upload of INSTRUCTION MEMORY
			
			-- Assignement of control signals to write 
			-- on the disired memory from external sources
			
			loadInstr	<= '1';
			readInstr	<= '0';
			ext_memRead	<= '0';
			ext_memWrite <= '0';
		
			i := 0;
			while not endfile(fileIstructions) loop
				
				extAddress_instr <= std_logic_vector(to_unsigned(i, 32));

				readline(fileIstructions, inLine_instruction);
				read(inLine_instruction, instruction);
				extData	<= instruction;
				
				i := i+1;
				wait for 1 ns;
			end loop;
			
		-- Upload of DATA MEMORY
		
			-- Assignement of control signals to write 
			-- on the disired memory from external sources
			
			loadInstr	<= '0';
			readInstr	<= '0';
			ext_memRead	<= '0';
			ext_memWrite 	<= '1';
			
			i := 264306688;
			while not endfile(fileData) loop
				
				extAddress_data <= std_logic_vector(to_unsigned(i, 32));

				readline(fileData, inLine_data);
				read(inLine_data, data);
				extData	<= data;
				
				i := i+1;
				wait for 1 ns;

			wait for 30 ns;
			loadInstr	<= '0';
			readInstr	<= '1';
			ext_memRead	<= '0';
			ext_memWrite 	<= '0';

			for i in 0 to 22 loop
				extAddress_instr <= std_logic_vector(to_unsigned(i, 32));
				wait for 1 ns;
			end loop;

			loadInstr	<= '0';
			readInstr	<= '0';
			ext_memRead	<= '1';
			ext_memWrite 	<= '0';
			wait for 20 ns;

			for i in 264306688 to 264306695 loop
				extAddress_data <= std_logic_vector(to_unsigned(i, 32));
				wait for 1 ns;
			end loop;
			end loop;
			
		-- RESET AND uP WORKING
		wait for 100 ns;
		rst	<= '1';
		wait for 5 ns;
		loadExt		<= '0';
		readInstr	<= '1';
		wait;
	end process data_gen;


end architecture test;
