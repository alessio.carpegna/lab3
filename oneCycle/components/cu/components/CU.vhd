library ieee;
use ieee.std_logic_1164.all;

entity CU is
	port(
		IM_instruction	: in std_logic_vector(31 downto 0);

		ALU_operation	: out std_logic_vector(3 downto 0);	
		ALU_src1	: out std_logic;
		ALU_src2	: out std_logic;
		regWrite	: out std_logic;
		memToReg	: out std_logic;
		branch		: out std_logic;
		jal		: out std_logic;
		memRead		: out std_logic;
		memWrite	: out std_logic
	);
end entity CU;

architecture behaviour of CU is

	-- array used to assign the values to the different
	-- outputs with a single operation
	signal instructions	: std_logic_vector(11 downto 0);

	signal opcode		: std_logic_vector(6 downto 0);
	signal funct3		: std_logic_vector(2 downto 0);
	signal funct7		: std_logic_vector(6 downto 0);

begin

	-- GENERATION OF STRINGS TO BE CHECKED
	opcode			<= IM_instruction(6 downto 0);
	funct3			<= IM_instruction(14 downto 12);
	funct7			<= IM_instruction(31 downto 25);
	
	-- CONNECTION BIT BY BIT TO THE OUTPUTS
	ALU_operation(3)	<= instructions(11);
	ALU_operation(2)	<= instructions(10);
	ALU_operation(1)	<= instructions(9);
	ALU_operation(0)	<= instructions(8);
	ALU_src1		<= instructions(7);
	ALU_src2		<= instructions(6);
	regWrite		<= instructions(5);
	memToReg		<= instructions(4);
	branch			<= instructions(3);
	jal			<= instructions(2);
	memRead			<= instructions(1);
	memWrite		<= instructions(0);

	decode : process(opcode, funct3, funct7)
	begin
		if (opcode & funct3 & funct7) = "01100110000000000" then
			instructions <= "001010100000"; -- ADD
			
		elsif (opcode & funct3) = "0010011000" then
			instructions <= "001011100000"; -- ADDI
			
		elsif (opcode) = "0010111" then
			instructions <= "101001100000"; -- AUIPC
			
		elsif (opcode) = "0110111" then
			instructions <= "010101100000"; -- LUI
			
		elsif (opcode & funct3) = "1100011000" then
			instructions <= "111110001000"; -- BEQ
			
		elsif (opcode & funct3) = "0000011010" then
			instructions <= "001011110010"; -- LW
			
		elsif (opcode & funct3 & funct7) = "00100111010100000" then
			instructions <= "001111100000"; -- SRAI
			
		elsif (opcode & funct3) = "0010011111" then
			instructions <= "000011100000"; -- ANDI
			
		elsif (opcode & funct3 & funct7) = "01100111000000000" then
			instructions <= "000110100000"; -- XOR
			
		elsif (opcode & funct3 & funct7) = "01100110100000000" then
			instructions <= "010010100000"; -- SLT
			
		elsif (opcode) = "1101111" then
			instructions <= "011001100100"; -- JAL
			
		elsif (opcode & funct3) = "0100011010" then
			instructions <= "001011010001"; -- SW
			
		else 	
			instructions <= "111111010000"; -- GENERAL CASE
		end if;
		
	end process decode;

end architecture behaviour;
