set DUT "../components/memory.vhd"
set TB "memory_tb"
set TB_DIR "../tb"

vcom $DUT
vcom $TB_DIR/$TB.vhd

vsim -t ns -novopt work.$TB
run 1 us

quit -f

