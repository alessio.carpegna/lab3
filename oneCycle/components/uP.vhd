library ieee;
use ieee.std_logic_1164.all;

entity uP is
	port(
		clk			: in std_logic;
		rst			: in std_logic;
		
		IM_instruction	: in std_logic_vector(31 downto 0);
		read_data		: in std_logic_vector(31 downto 0);

		ALU_result	: out std_logic_vector(31 downto 0);
		writeData	: out std_logic_vector(31 downto 0);
		PC			: out std_logic_vector(31 downto 0);	
		memWrite	: out std_logic;
		memRead		: out std_logic;
		
		--- per tb ---
		tb_CU_istructions: out std_logic_vector(11 downto 0)
		--------------
	);
end entity uP;

architecture behaviour of uP is

	component Datapath
		port(
			clk				: in std_logic;
			rst				: in std_logic;
			
			IM_instruction	: in std_logic_vector(31 downto 0);  -- From Instruction of instruction mem to uP
			read_data		: in std_logic_vector(31 downto 0);  -- From read data of data mem to "memToReg" MUX of uP
			ALU_operation	: in std_logic_vector(3 downto 0);	 -- From CU
			ALU_src1		: in std_logic;						 -- From CU
			ALU_src2		: in std_logic;						 -- From CU
			regWrite		: in std_logic;						 -- From CU
			memToReg		: in std_logic;						 -- From CU
			branch			: in std_logic;						 -- From CU
			jal				: in std_logic;						 -- From CU: OR with the out of the and gate to select the mux PC_SRC 
			
			pc				: out std_logic_vector(31 downto 0); -- From PC of uP to read address of instruction mem
			ALU_result		: out std_logic_vector(31 downto 0); -- From ALU result of uP to address of data memory
			read_data2		: out std_logic_vector(31 downto 0)  -- From Immediate generator of uP to Write Data of data memory
		);
	end component Datapath;
	
	component CU 
		port(
			IM_instruction	: in std_logic_vector(31 downto 0);

			ALU_operation	: out std_logic_vector(3 downto 0);	
			ALU_src1		: out std_logic;
			ALU_src2		: out std_logic;
			regWrite		: out std_logic;
			memToReg		: out std_logic;
			branch			: out std_logic;
			jal				: out std_logic;
			memRead			: out std_logic;
			memWrite		: out std_logic	
		);
	end component CU;

signal read_data_int		: std_logic_vector(31 downto 0);
signal ALU_operation_int	: std_logic_vector(3 downto 0);
signal ALU_src1_int			: std_logic;
signal ALU_src2_int			: std_logic;
signal regWrite_int			: std_logic;
signal memToReg_int			: std_logic;
signal branch_int			: std_logic;
signal jal_int				: std_logic;

--- per tb ---
signal memRead_int, memWrite_int	: std_logic;
--------------

begin

	------- per tb -------
	memRead <= memRead_int;
	memWrite <= memWrite_int;
	tb_CU_istructions <= ALU_operation_int & ALU_src1_int & ALU_src2_int & regWrite_int &memToReg_int & branch_int & jal_int & memRead_int & memWrite_int;
	------------------------

	DP:	Datapath port map(
	
		clk		=> clk,
		rst		=> rst,
		
		IM_instruction	=> IM_instruction,
		read_data		=> read_data,
		ALU_operation	=> ALU_operation_int,
		ALU_src1		=> ALU_src1_int,
		ALU_src2		=> ALU_src2_int,
		regWrite		=> regWrite_int,
		memToReg		=> memToReg_int,
		branch			=> branch_int,
		jal				=> jal_int,
		
		pc				=> pc,
		ALU_result		=> ALU_result,
		read_data2		=> writeData
	);
	
	CONTROL_UNIT: CU port map(
	
		IM_instruction	=> IM_instruction,
		
		ALU_operation	=> ALU_operation_int,
		ALU_src1		=> ALU_src1_int,
		ALU_src2		=> ALU_src2_int,
		regWrite		=> regWrite_int,
		memToReg		=> memToReg_int,
		branch			=> branch_int,
		jal				=> jal_int,
		memRead			=> memRead_int,
		memWrite		=> memWrite_int
	);

end architecture behaviour;
