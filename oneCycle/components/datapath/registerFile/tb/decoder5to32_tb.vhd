library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity decoder5to32_tb is
end entity decoder5to32_tb;

architecture test of decoder5to32_tb is

	 component decoder5to32 is
		port(
			-- input signal
			encodedIn	: in std_logic_vector(4 downto 0);

			-- output signal
			decodedOut	: out std_logic_vector(31 downto 0)
		);
	end component decoder5to32;

	signal encodedIn	: std_logic_vector(4 downto 0);
	signal decodedOut	: std_logic_vector(31 downto 0);
begin


	testing : process
	begin
		for i in 0 to 31 loop
			encodedIn <= std_logic_vector(to_unsigned(i,5));
			wait for 10 ns;
		end loop;
		wait;
	end process testing;

	DUT : decoder5to32
		port map(
			-- input signal
			encodedIn	=> encodedIn,
			-- output signal
			decodedOut	=> decodedOut		
		);

end architecture test;
