library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity regFile32_1w_2r is
	generic(
		regParallelism	: integer := 32		
	);
	port(
		-- input signals
		writeAddress	: in std_logic_vector(4 downto 0);
		readAddress1	: in std_logic_vector(4 downto 0);
		readAddress2	: in std_logic_vector(4 downto 0);
		dataIn		: in signed(regParallelism-1 downto 0);
		writeEn		: in std_logic;
		clk		: in std_logic;

		-- output signals
		readReg1	: out signed(regParallelism-1 downto 0);
		readReg2	: out signed(regParallelism-1 downto 0)

	);
end entity regFile32_1w_2r;

architecture behaviour of regFile32_1w_2r is

	-- input decoder
	component decoder is
		port(
			-- input signal
			encodedIn	: in std_logic_vector(4 downto 0);

			-- output signals
			decodedOut	: out std_logic_vector(31 downto 0)
		);
	end component  decoder;


	-- output multiplexers
	component mux32to1 is
		generic(
			-- inputs parallelism
			L_inputs	: integer := 32		
		);
		port(
			-- input signals
			sel		: in std_logic_vector(4 downto 0);
			muxIn0		: in signed(L_inputs-1 downto 0);
			muxIn1		: in signed(L_inputs-1 downto 0);
			muxIn2		: in signed(L_inputs-1 downto 0);
			muxIn3		: in signed(L_inputs-1 downto 0);
			muxIn4		: in signed(L_inputs-1 downto 0);
			muxIn5		: in signed(L_inputs-1 downto 0);
			muxIn6		: in signed(L_inputs-1 downto 0);
			muxIn7		: in signed(L_inputs-1 downto 0);
			muxIn8		: in signed(L_inputs-1 downto 0);
			muxIn9		: in signed(L_inputs-1 downto 0);
			muxIn10		: in signed(L_inputs-1 downto 0);
			muxIn11		: in signed(L_inputs-1 downto 0);
			muxIn12		: in signed(L_inputs-1 downto 0);
			muxIn13		: in signed(L_inputs-1 downto 0);
			muxIn14		: in signed(L_inputs-1 downto 0);
			muxIn15		: in signed(L_inputs-1 downto 0);
			muxIn16		: in signed(L_inputs-1 downto 0);
			muxIn17		: in signed(L_inputs-1 downto 0);
			muxIn18		: in signed(L_inputs-1 downto 0);
			muxIn19		: in signed(L_inputs-1 downto 0);
			muxIn20		: in signed(L_inputs-1 downto 0);
			muxIn21		: in signed(L_inputs-1 downto 0);
			muxIn22		: in signed(L_inputs-1 downto 0);
			muxIn23		: in signed(L_inputs-1 downto 0);
			muxIn24		: in signed(L_inputs-1 downto 0);
			muxIn25		: in signed(L_inputs-1 downto 0);
			muxIn26		: in signed(L_inputs-1 downto 0);
			muxIn27		: in signed(L_inputs-1 downto 0);
			muxIn28		: in signed(L_inputs-1 downto 0);
			muxIn29		: in signed(L_inputs-1 downto 0);
			muxIn30		: in signed(L_inputs-1 downto 0);
			muxIn31		: in signed(L_inputs-1 downto 0);

			--output signal
			muxOut		: out signed(L_inputs-1 downto 0)
		);
	end component  mux32to1;


	-- generic register
	component regBeh is
		generic(
			-- parallelism of the register
			N	: integer := 32
		);
		port(
			-- input signals
			clk	: in std_logic;
			en	: in std_logic;
			regIn	: in signed(N-1 downto 0);

			-- output signals
			regOut	: out signed(N-1 downto 0)
		);
	end component regBeh;

	type matrix32_32 is array(1 to 31) of signed(31 downto 0);
	
	signal decodedOut	: std_logic_vector(31 downto 0);
	signal regEnable	: std_logic_vector(31 downto 0);
	signal muxIn		: matrix32_32;
	
begin

	-- array of register
	registers1to31 : for i in 1 to 31 generate

		regEnable(i) <= writeEn and decodedOut(i);

		xi : regBeh
			generic map(
				N	=> 32
			)
			port map(
				-- input signals
				clk	=> clk,
				en	=> regEnable(i),
				regIn	=> dataIn,

				-- output signals
				regOut	=> muxIn(i)		
			);
	end generate registers1to31;


	inputDecoder : decoder
		port map(
			-- input signal
			encodedIn	=> writeAddress,
			decodedOut	=> decodedOut
		);

	outputMux1 : mux32to1
		generic map(
			L_inputs	=> 32		
		)
		port map(
			-- input signals
			sel		=> readAddress1,
			muxIn0		=> (others=>'0'),
			muxIn1		=> muxIn(1),
			muxIn2		=> muxIn(2),
			muxIn3		=> muxIn(3),
			muxIn4		=> muxIn(4),
			muxIn5		=> muxIn(5),
			muxIn6		=> muxIn(6),
			muxIn7		=> muxIn(7),
			muxIn8		=> muxIn(8),
			muxIn9		=> muxIn(9),
			muxIn10		=> muxIn(10),
			muxIn11		=> muxIn(11),
			muxIn12		=> muxIn(12),
			muxIn13		=> muxIn(13),
			muxIn14		=> muxIn(14),
			muxIn15		=> muxIn(15),
			muxIn16		=> muxIn(16),
			muxIn17		=> muxIn(17),
			muxIn18		=> muxIn(18),
			muxIn19		=> muxIn(19),
			muxIn20		=> muxIn(20),
			muxIn21		=> muxIn(21),
			muxIn22		=> muxIn(22),
			muxIn23		=> muxIn(23),
			muxIn24		=> muxIn(24),
			muxIn25		=> muxIn(25),
			muxIn26		=> muxIn(26),
			muxIn27		=> muxIn(27),
			muxIn28		=> muxIn(28),
			muxIn29		=> muxIn(29),
			muxIn30		=> muxIn(30),
			muxIn31		=> muxIn(31),

			-- output signal
			muxOut		=> readReg1
		);

	outputMux2 : mux32to1
		generic map(
			L_inputs	=> 32		
		)
		port map(
			-- input signals
			sel		=> readAddress2,
			muxIn0		=> (others => '0'),
			muxIn1		=> muxIn(1),
			muxIn2		=> muxIn(2),
			muxIn3		=> muxIn(3),
			muxIn4		=> muxIn(4),
			muxIn5		=> muxIn(5),
			muxIn6		=> muxIn(6),
			muxIn7		=> muxIn(7),
			muxIn8		=> muxIn(8),
			muxIn9		=> muxIn(9),
			muxIn10		=> muxIn(10),
			muxIn11		=> muxIn(11),
			muxIn12		=> muxIn(12),
			muxIn13		=> muxIn(13),
			muxIn14		=> muxIn(14),
			muxIn15		=> muxIn(15),
			muxIn16		=> muxIn(16),
			muxIn17		=> muxIn(17),
			muxIn18		=> muxIn(18),
			muxIn19		=> muxIn(19),
			muxIn20		=> muxIn(20),
			muxIn21		=> muxIn(21),
			muxIn22		=> muxIn(22),
			muxIn23		=> muxIn(23),
			muxIn24		=> muxIn(24),
			muxIn25		=> muxIn(25),
			muxIn26		=> muxIn(26),
			muxIn27		=> muxIn(27),
			muxIn28		=> muxIn(28),
			muxIn29		=> muxIn(29),
			muxIn30		=> muxIn(30),
			muxIn31		=> muxIn(31),

			-- output signal
			muxOut		=> readReg2

		);


end architecture behaviour;
