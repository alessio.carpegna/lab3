library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity decoder is
	generic(
		N_inputs	: integer := 5	
	);
	port(
		-- input signal
		encodedIn	: in std_logic_vector(N_inputs-1 downto 0);

		--output signal
		decodedOut	: out std_logic_vector(2**N_inputs-1 downto 0)	
	);
end entity decoder;

architecture behaviour of decoder is
begin

	decode : process(encodedIn)
	begin
		-- default value
		decodedOut <= (others => '0');
		decodedOut(to_integer(unsigned(encodedIn))) <= '1';
	end process decode;

end architecture behaviour;
