set DUT "../components/regFile32_1w_2r.vhd"
set TB "regFile32_1w_2r_tb"
set TB_DIR "../tb"

vcom $DUT
vcom $TB_DIR/$TB.vhd

vsim -t ns -novopt work.$TB
run 1us

quit -f
