library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity srai is
	generic(
		shamtLength	: integer := 5
			
	);
	port(
		-- Value to shift
		rs1	: in signed(2**shamtLength-1 downto 0);
		-- Shift amount (shamt)
		imm	: in signed(shamtLength-1 downto 0);
		
		-- Output signals
		sraiOut	: out signed(2**shamtLength-1 downto 0)	
	);
end entity srai;

architecture behaviour of srai is
begin

	shift : process(rs1, imm)
	begin
		if(imm=to_signed(0, shamtLength))
		then
			-- no shift
			sraiOut <= rs1;
		else
			-- "shamt" MSBs of sraiOut = MSB of rs1 
			sraiOut(sraiOut'length-1 downto 
					sraiOut'length-to_integer(unsigned(imm))) <= 
			(others => rs1(rs1'length-1));

			-- "length - shamt" LSBs of sraiOut = "length - shamt" MSBs of rs1
			sraiOut(sraiOut'length-to_integer(unsigned(imm))-1 
				downto 0) <= 
			rs1(rs1'length-1 downto to_integer(unsigned(imm)));
		end if;
	end process shift;

end architecture behaviour;
