library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity xor_block is 

	port(
		rs1		: in signed(31 downto 0);
		rs2		: in signed(31 downto 0);
		
		xor_out	: out signed(31 downto 0)
	);
end entity xor_block;

architecture structure of xor_block is 

begin
	xor_out <= rs1 XOR rs2;

end architecture structure;