library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity mux_operation is 

	port(
		sel		: in signed(3 downto 0);
		in0		: in signed(31 downto 0);
		in1		: in signed(31 downto 0);
		in2		: in signed(31 downto 0);
		in3		: in signed(31 downto 0);
		in4		: in signed(31 downto 0);
		in5		: in signed(31 downto 0);
		in6		: in signed(31 downto 0);
		in7		: in signed(31 downto 0);
		in8		: in signed(31 downto 0);
		in9		: in signed(31 downto 0);
		in10	: in signed(31 downto 0);
		in11	: in signed(31 downto 0);
		in12	: in signed(31 downto 0);
		in13	: in signed(31 downto 0);
		in14	: in signed(31 downto 0);
		in15	: in signed(31 downto 0);
		
		mux_out	: out signed(31 downto 0)
	);
end entity mux_operation;

architecture structure of mux_operation is 

begin
	
	selection: process(sel, in0, in1, in2, in3, in4, in5, in6, in7, in8, in9, in10, in11, in12, in13, in14, in15)
	begin
		if sel = "0000" then
			mux_out	<= in0;
		elsif sel = "0001" then
			mux_out	<= in1;
		elsif sel = "0010" then	
			mux_out	<= in2;
		elsif sel = "0011" then 
			mux_out	<= in3;
		elsif sel = "0100" then
			mux_out	<= in4;
		elsif sel = "0101" then	-- MSB DEVE essere 0 per girare mux_adder per somma normale
			mux_out	<= in5;
		elsif sel = "0110" then
			mux_out	<= in6;
		elsif sel = "0111" then
			mux_out	<= in7;
		elsif sel = "1000" then
			mux_out	<= in8;
		elsif sel = "1001" then
			mux_out	<= in9;
		elsif sel = "1010" then
			mux_out	<= in10;
		elsif sel = "1011" then
			mux_out	<= in11;
		elsif sel = "1100" then
			mux_out	<= in12;
		elsif sel = "1101" then
			mux_out	<= in13;
		elsif sel = "1110" then
			mux_out	<= in14;
		elsif sel = "1111" then
			mux_out	<= in15;
		else
			mux_out	<= (others => '0');
		end if;
	end process selection;

end architecture structure;