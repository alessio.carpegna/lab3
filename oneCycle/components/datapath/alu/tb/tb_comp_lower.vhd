library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use std.textio.all;
use ieee.std_logic_textio.all;



entity tb_comp_lower is
end entity tb_comp_lower;

architecture simulation of tb_comp_lower is

component comp_lower is
port(
	-- Ingressi
	rs1,rs2		: in signed(31 downto 0);
	-- Uscita
	comp_lower_out	: out signed(31 downto 0)
		
);
end component comp_lower;

signal test_rs1,test_rs2	: signed(31 downto 0);
signal test_comp_lower_out	: signed(31 downto 0);
file outFile	: text open write_mode is "results_comp_lower.txt";
begin

DUT : comp_lower port map(
	rs1		=> test_rs1,		
	rs2		=> test_rs2,
	comp_lower_out	=> test_comp_lower_out
);

	data_gen : process


		variable outline		: line;
		variable var_comp_lower_out	: signed(31 downto 0);
		
		begin
			-- input generation
			test_rs1 <= "00000000000000000000000000000000";
		       	test_rs2 <= "00000000000000000000000000000000";
			wait for 10 ns;
			test_rs1 <= "00000000000100000000000000000000";
		       	test_rs2 <= "00000000000000000000011100000000";
			wait for 10 ns;
			test_rs1 <= "11000000000000000000000000000000";
		       	test_rs2 <= "00000000011100000000000000000000";
			wait for 10 ns;
			test_rs1 <= "10001000000000000000000000000000";
		       	test_rs2 <= "11100000000000000000000000000000";
			wait;
		end process data_gen;


end architecture simulation;
