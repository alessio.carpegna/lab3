library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use std.textio.all;
use ieee.std_logic_textio.all;

entity tb_comp_equal is
end entity tb_comp_equal;

architecture simulation of tb_comp_equal is

component comp_equal 
port(	
	-- Inputs from register file
	rs1	: in signed(31 downto 0);
	rs2	: in signed(31 downto 0);
	-- Uscita
	zero	: out std_logic	
);
end component comp_equal;

signal test_rs1,test_rs2	: signed(31 downto 0):= (others=>'0');
signal test_zero		: std_logic :='0';

file inFile			: text open read_mode is "comp_equal_inputs.txt";
file outFile			: text open write_mode is "results_comp_equal.txt";

begin

DUT : comp_equal port map(
	rs1	=> test_rs1,	
	rs2	=> test_rs2,	
	zero	=> test_zero	
);




		data_gen : process

		variable inLine		: line;
		variable outline	: line;

		variable rs1_read	: std_logic_vector(31 downto 0);
		variable rs2_read	: std_logic_vector(31 downto 0);
		variable zero_var	: std_logic := '0';
		
		begin
			if not endfile(inFile)
			then
			-- input generation
				readline(inFile,inLine);
				read(inline,rs1_read);
				test_rs1	<= signed(rs1_read);
				read(inLine,rs2_read);
				test_rs2	<= signed(rs2_read);
			-- output writing
				wait for 10 ns;
				zero_var:= test_zero;
				write(outLine,zero_var);
				writeline(outFile,outLine);
			else 
				wait;

			end if;
		end process data_gen;

end architecture simulation;
