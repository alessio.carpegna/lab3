#!/usr/bin/python3

import sys
import random 
import os

modelsim_file = "comp_lower.do"		#file generato per lanciare la simulazione su Modelsim



# Il primo argomento da passare allo script è il percorso completo (con anche il nome del file) del testbench
# gli altri argomenti sono i file necessari alla compilazione del testbench, ovvero tutti i file che componegono il progetto

# ESEMPIO:	./questo_script.py /generic_path/testbench_name.vhd /generic_path/file1.vhd /generic_path/file2.vhd


tb_file_name = sys.argv[1].split("/")[-1]		#salvo il nome del file testbench
tb_path = "/".join(sys.argv[1].split("/")[:-1])		#salvo il percorso del testbench per metterci il file di ingressi generati

f_mod = open(modelsim_file,'w')
mod_command=""
for k in sys.argv[1:]:
	mod_command=mod_command + "vcom -93 -work ./work "+k+"\n"


mod_command=mod_command+"vsim -t ns -novopt work.tb_comp_lower\n"+"run 100 us\n"+"quit -f\n"
f_mod.write(mod_command)
