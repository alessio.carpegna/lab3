library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity datapath is 

	port(
	
		clk				: in std_logic;
		rst				: in std_logic;
		
		IM_instruction	: in std_logic_vector(31 downto 0);  -- From Instruction of instruction mem to uP
		read_data		: in std_logic_vector(31 downto 0);  -- From read data of data mem to "memToReg" MUX of uP
		ALU_operation	: in std_logic_vector(3 downto 0);	 -- From CU
		ALU_src1		: in std_logic;						 -- From CU
		ALU_src2		: in std_logic;						 -- From CU
		regWrite		: in std_logic;						 -- From CU
		memToReg		: in std_logic;						 -- From CU
		branch			: in std_logic;						 -- From CU
		jal				: in std_logic;						 -- From CU: OR with the out of the and gate to select the mux PC_SRC 
		
		pc				: out std_logic_vector(31 downto 0); -- From PC of uP to read address of instruction mem
		ALU_result		: out std_logic_vector(31 downto 0); -- From ALU result of uP to address of data memory
		read_data2		: out std_logic_vector(31 downto 0)  -- From register file generator of uP to Write Data of data memory
	);
end entity datapath;

architecture behavioral of datapath is 

	component alu
		port(
			alu_operation	: in signed(3 downto 0);
			rs1				: in signed(31 downto 0);
			rs2				: in signed(31 downto 0);
			
			rd		: out signed(31 downto 0);
			zero	: out std_logic
		);
	end component alu;
	
	component regFile32_1w_2r
		generic(regParallelism	: integer := 32);
		port(
			-- input signals
			writeAddress	: in std_logic_vector(4 downto 0);
			readAddress1	: in std_logic_vector(4 downto 0);
			readAddress2	: in std_logic_vector(4 downto 0);
			dataIn			: in signed(regParallelism-1 downto 0);
			writeEn			: in std_logic;
			clk				: in std_logic;

			-- output signals
			readReg1	: out signed(regParallelism-1 downto 0);
			readReg2	: out signed(regParallelism-1 downto 0)
		);
	end component regFile32_1w_2r;

	component adder_32bit
		port(
			add1	: in signed(31 downto 0);
			add2	: in signed(31 downto 0);
			
			sum_out	: out signed(31 downto 0)
		);
	end component adder_32bit;
	
	component mux_2to1
		port(
			sel		: in std_logic;
			in0		: in signed(31 downto 0);
			in1		: in signed(31 downto 0);
			
			mux_out	: out signed(31 downto 0)
		);
	end component mux_2to1;
	
	component immGen
		generic(parallelism	: integer := 32);
		port(
			-- input signal
			instruction	: in std_logic_vector(parallelism-1 downto 0);

			-- output signal
			immediate	: out signed(parallelism-1 downto 0)
		);
	end component immGen;
	
	component register_32bit_rst	
	port(
		--input signals
		clk			: in std_logic;
		reg_en		: in std_logic;
		rst			: in std_logic;
		d_in		: in signed(31 downto 0);

		--output signals
		d_out		: out signed(31 downto 0)
	);
	end component register_32bit_rst;

	--------------- SIGNALS ---------------
	
	signal read_data1_int, read_data2_int, alu_result_int, pc_out, pc_seq, pc_jump, immediate : signed(31 downto 0);
	
	signal mux_alu_src1, mux_alu_src2 , mux_memToReg, mux_PCsrc: signed(31 downto 0);
	
	signal zero_int, mux_PCsrc_sel : std_logic;
	signal shifted_imm: signed(31 downto 0);
	
	---------------------------------------
	
begin

	pc			<= std_logic_vector(pc_out);
	ALU_result	<= std_logic_vector(alu_result_int);
	read_data2	<= std_logic_vector(read_data2_int);
	
	mux_PCsrc_sel	<= jal OR (zero_int AND branch);
	
	shifted_imm(0)	<= '0';
	shifted_imm(31 downto 1) <= immediate(30 downto 0);

	ALU_BLOCK: alu port map(
		
		alu_operation	=> signed(ALU_operation),
		rs1				=> mux_alu_src1,
		rs2				=> mux_alu_src2,
		
		rd				=> alu_result_int,
		zero			=> zero_int		
	);
	
	REGISTER_FILE: regFile32_1w_2r generic map(regParallelism => 32)
		port map(
			clk				=> clk,
			writeAddress	=> IM_instruction(11 downto 7),
			readAddress1	=> IM_instruction(19 downto 15),
			readAddress2	=> IM_instruction(24 downto 20),
			dataIn			=> mux_memToReg,
			writeEn			=> regWrite,
			
			readReg1		=> read_data1_int,
			readReg2		=> read_data2_int
		);
		
	PROGRAM_COUNTER: register_32bit_rst port map(
		clk		=> clk,
		reg_en	=> '1',
		rst		=> rst,
		d_in	=> mux_PCsrc,
		
		d_out	=> pc_out
	);
	
	MUX_MEM_TO_REG: mux_2to1 port map(
		sel	=> memToReg,
		in1	=> signed(read_data),
		in0	=> alu_result_int,
		
		mux_out	=> mux_memToReg
	);
	
	MUX_PC_SRC: mux_2to1 port map(
		sel	=> mux_PCsrc_sel, 
		in0	=> pc_seq,
		in1	=> pc_jump,
		
		mux_out	=> mux_PCsrc
	);
	
	MUX_ALU_PC_DATA1: mux_2to1 port map(
		sel	=> ALU_src1, 
		in0	=> pc_out,
		in1	=> read_data1_int,
		
		mux_out	=> mux_alu_src1
	);
	
	MUX_ALU_IMM_DATA2: mux_2to1 port map(
		sel	=> ALU_src2, 
		in0	=> read_data2_int,
		in1	=> immediate,
		
		mux_out	=> mux_alu_src2
	);
	
	ADDER_PC: adder_32bit port map(
		add1	=> pc_out,
		add2	=> "00000000000000000000000000000100",
		sum_out	=> pc_seq
	);
	
	ADDER_JUMP: adder_32bit port map(
		add1	=> pc_out,
		add2	=> shifted_imm,
		
		sum_out	=> pc_jump
	);
	
	IMM_GENERATION: immGen port map(
		instruction	=> std_logic_vector(IM_instruction),
		immediate	=> immediate
	);

end architecture behavioral;