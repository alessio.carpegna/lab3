library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use std.textio.all;
use ieee.std_logic_textio.all;

entity tb_immGen is
end entity tb_immGen;

architecture test of tb_immGen is

	component immGen is
		generic(
			parallelism	: integer := 32		
		);
		port(
			-- input signal
			instruction	: in std_logic_vector(parallelism-1 downto 0);

			-- output signal
			immediate	: out signed(parallelism-1 downto 0)
		);
	end component immGen;

	signal instruction	: std_logic_vector(31 downto 0);
	signal immediate	: signed(31 downto 0);

	--file outFile		: text open write_mode is "immGen_results.txt";

begin

	testing : process

		variable outLine	: line;

	begin

--					    	I
--			       ----------------------------------
		instruction <= "00000000110000000000011111101111";
		wait for 10 ns;
		--write(outLine, std_logic_vector(immediate), right, 32);
		--writeline(outFile, outLine);

--						I
--			       ----------------------------------
		instruction <= "00000000001000000000000000000011";
		wait for 10 ns;
		--write(outLine, std_logic_vector(immediate), right, 32);
		--writeline(outFile, outLine);

--						S
--			       ----------------------------------
		instruction <= "10000010000000000000100010100011";
		wait for 10 ns;
		--write(outLine, std_logic_vector(immediate), right, 32);
		--writeline(outFile, outLine);

--						SB
--			       ----------------------------------
		instruction <= "10000100000000000000100111100011";
		wait for 10 ns;
		--write(outLine, std_logic_vector(immediate), right, 32);
		--writeline(outFile, outLine);

--						U
--			       ----------------------------------
		instruction <= "10000000000000000001000000010111";
		wait for 10 ns;
		--write(outLine, std_logic_vector(immediate), right, 32);
		--writeline(outFile, outLine);

--						U
--			       ----------------------------------
		instruction <= "01000000000000000010000000110111";
		wait for 10 ns;
		--write(outLine, std_logic_vector(immediate), right, 32);
		--writeline(outFile, outLine);

--						UJ
--			       ----------------------------------
		instruction <= "11000000001011001100000001001111";
		wait for 10 ns;
		--write(outLine, std_logic_vector(immediate), right, 32);
		--writeline(outFile, outLine);

		wait;

	end process testing;

	DUT : immGen
		generic map(
			parallelism	=> 32		
		)
		port map(
			-- input signal
			instruction	=> instruction,

			-- output signal
			immediate	=> immediate		
		);

end architecture test;
